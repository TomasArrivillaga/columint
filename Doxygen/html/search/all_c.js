var searchData=
[
  ['phc_5fcheckaverage',['phc_CheckAverage',['../group__photocell__module.html#gaa7bd4469987f911b7454ece791e1b79b',1,'phc_CheckAverage():&#160;photocell.c'],['../group__photocell__module.html#gaa7bd4469987f911b7454ece791e1b79b',1,'phc_CheckAverage():&#160;photocell.c']]],
  ['phc_5fcheckmeasure',['phc_CheckMeasure',['../group__photocell__module.html#ga2083d315c03df45b17f41a13bac1ade7',1,'phc_CheckMeasure():&#160;photocell.c'],['../group__photocell__module.html#ga2083d315c03df45b17f41a13bac1ade7',1,'phc_CheckMeasure():&#160;photocell.c']]],
  ['phc_5fgetlastvalue',['phc_GetLastValue',['../group__photocell__module.html#ga5481c072fc46a00e86e7cdf74555235c',1,'phc_GetLastValue(void):&#160;photocell_hw.c'],['../group__photocell__module.html#ga5481c072fc46a00e86e7cdf74555235c',1,'phc_GetLastValue(void):&#160;photocell_hw.c']]],
  ['phc_5finit',['phc_init',['../group__photocell__module.html#ga8ad85ce0c5ee02bf2ce3d15b3851f9d4',1,'phc_init():&#160;photocell_hw.c'],['../group__photocell__module.html#ga8ad85ce0c5ee02bf2ce3d15b3851f9d4',1,'phc_init():&#160;photocell_hw.c']]],
  ['phctriggermeasure',['phcTriggerMeasure',['../group__photocell__module.html#gaed99781fc344c6f94006a51c36c68ced',1,'phcTriggerMeasure(void):&#160;photocell_hw.c'],['../group__photocell__module.html#gaed99781fc344c6f94006a51c36c68ced',1,'phcTriggerMeasure(void):&#160;photocell_hw.c']]],
  ['photocell_2eh',['photocell.h',['../photocell_8h.html',1,'']]],
  ['photocell_5fhw_2eh',['photocell_hw.h',['../photocell__hw_8h.html',1,'']]],
  ['photocell_5fmodule',['Photocell_module',['../group__photocell__module.html',1,'']]],
  ['pwm_5fconfig',['pwm_config',['../group__output__module.html#ga0439b5ad2636e4feb57f92a67484d714',1,'pwm_config():&#160;output_hw.c'],['../group__output__module.html#ga0439b5ad2636e4feb57f92a67484d714',1,'pwm_config():&#160;output_hw.c']]],
  ['pwm_5fset_5foutput',['pwm_set_output',['../group__output__module.html#ga31cc6e721b0e10f066c92eae5f886fd8',1,'pwm_set_output(float duty_cycle):&#160;output_hw.c'],['../group__output__module.html#ga31cc6e721b0e10f066c92eae5f886fd8',1,'pwm_set_output(float duty_cycle):&#160;output_hw.c']]]
];
