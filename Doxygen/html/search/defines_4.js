var searchData=
[
  ['reg_5factpwr',['REG_ACTPWR',['../acs71020__hw_8h.html#ac7ae086dd70f63fcd4a767e941392a6a',1,'acs71020_hw.h']]],
  ['reg_5factpwr_5fprom',['REG_ACTPWR_PROM',['../acs71020__hw_8h.html#ac855d9a8aafd3a88b8d75849b899602b',1,'acs71020_hw.h']]],
  ['reg_5fapppwr',['REG_APPPWR',['../acs71020__hw_8h.html#a15f814d65493b9fcae3afb048b823edc',1,'acs71020_hw.h']]],
  ['reg_5firms_5fvrms',['REG_IRMS_VRMS',['../acs71020__hw_8h.html#a569a19db90beb857919c6004323f6163',1,'acs71020_hw.h']]],
  ['reg_5firms_5fvrms_5fprom',['REG_IRMS_VRMS_PROM',['../acs71020__hw_8h.html#a1e257199502f1e84e501b2cff982acc3',1,'acs71020_hw.h']]],
  ['reg_5fpfactor',['REG_PFACTOR',['../acs71020__hw_8h.html#ad0ca04e9a41b9d695abaea33300d046b',1,'acs71020_hw.h']]],
  ['reg_5freapwr',['REG_REAPWR',['../acs71020__hw_8h.html#a4671d2f9003be225428105aa201cd5dc',1,'acs71020_hw.h']]]
];
