var searchData=
[
  ['sendcommandandwaitforok',['sendCommandAndWaitForOk',['../group___lo_ra__module.html#ga7607411927e8cc8133f82d7643b94022',1,'sendCommandAndWaitForOk(char *stringToSend):&#160;LORA.c'],['../group___lo_ra__module.html#ga7607411927e8cc8133f82d7643b94022',1,'sendCommandAndWaitForOk(char *stringToSend):&#160;LORA.c']]],
  ['strcat',['strcat',['../utils_8h.html#aa9dc54366095b11a3780d3b17a579a29',1,'utils.h']]],
  ['strcmp',['strcmp',['../utils_8h.html#a11bd144d7d44914099a3aeddf1c8567d',1,'utils.c']]],
  ['strcpy',['strcpy',['../utils_8h.html#a7a82515b5d377be04817715c5465b647',1,'utils.c']]],
  ['sun_5fequation_5fcalc_5fsun_5fevents',['sun_equation_calc_sun_events',['../group__sun__equation__module.html#gaa6891d9e97494cf08a941f9ef78cccf8',1,'sun_equation_calc_sun_events(float lat, float lon, int time_zone):&#160;sun_equation.c'],['../group__sun__equation__module.html#gaa6891d9e97494cf08a941f9ef78cccf8',1,'sun_equation_calc_sun_events(float lat, float lon, int time_zone):&#160;sun_equation.c']]],
  ['sun_5fequation_5fget_5fsunrise',['sun_equation_get_sunrise',['../group__sun__equation__module.html#ga46619218cf911a9029408a33e63d4c3e',1,'sun_equation_get_sunrise(type_time *sunrise_aux):&#160;sun_equation.c'],['../group__sun__equation__module.html#ga46619218cf911a9029408a33e63d4c3e',1,'sun_equation_get_sunrise(type_time *sunrise_aux):&#160;sun_equation.c']]],
  ['sun_5fequation_5fget_5fsunset',['sun_equation_get_sunset',['../group__sun__equation__module.html#ga1df1b8069ec0dbf162b7e1f6e5bc7af1',1,'sun_equation_get_sunset(type_time *sunset_aux):&#160;sun_equation.c'],['../group__sun__equation__module.html#ga1df1b8069ec0dbf162b7e1f6e5bc7af1',1,'sun_equation_get_sunset(type_time *sunset_aux):&#160;sun_equation.c']]]
];
