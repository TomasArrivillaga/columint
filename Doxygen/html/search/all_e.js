var searchData=
[
  ['sendcommandandwaitforok',['sendCommandAndWaitForOk',['../group___lo_ra__module.html#ga7607411927e8cc8133f82d7643b94022',1,'sendCommandAndWaitForOk(char *stringToSend):&#160;LORA.c'],['../group___lo_ra__module.html#ga7607411927e8cc8133f82d7643b94022',1,'sendCommandAndWaitForOk(char *stringToSend):&#160;LORA.c']]],
  ['slave_5faddress',['SLAVE_ADDRESS',['../group__acs71020__module.html#gae2f0ff6faf548539a21b93a034e278e8',1,'acs71020_hw.h']]],
  ['strcat',['strcat',['../utils_8h.html#aa9dc54366095b11a3780d3b17a579a29',1,'utils.h']]],
  ['strcmp',['strcmp',['../utils_8h.html#a11bd144d7d44914099a3aeddf1c8567d',1,'utils.c']]],
  ['strcpy',['strcpy',['../utils_8h.html#a7a82515b5d377be04817715c5465b647',1,'utils.c']]],
  ['struct_5fdate',['struct_date',['../structstruct__date.html',1,'']]],
  ['struct_5fevent',['struct_event',['../structstruct__event.html',1,'']]],
  ['struct_5fmoment',['struct_moment',['../structstruct__moment.html',1,'']]],
  ['struct_5ftime',['struct_time',['../structstruct__time.html',1,'']]],
  ['sun_5fequation_2eh',['sun_equation.h',['../sun__equation_8h.html',1,'']]],
  ['sun_5fequation_5fcalc_5fsun_5fevents',['sun_equation_calc_sun_events',['../group__sun__equation__module.html#gaa6891d9e97494cf08a941f9ef78cccf8',1,'sun_equation_calc_sun_events(float lat, float lon, int time_zone):&#160;sun_equation.c'],['../group__sun__equation__module.html#gaa6891d9e97494cf08a941f9ef78cccf8',1,'sun_equation_calc_sun_events(float lat, float lon, int time_zone):&#160;sun_equation.c']]],
  ['sun_5fequation_5fget_5fsunrise',['sun_equation_get_sunrise',['../group__sun__equation__module.html#ga46619218cf911a9029408a33e63d4c3e',1,'sun_equation_get_sunrise(type_time *sunrise_aux):&#160;sun_equation.c'],['../group__sun__equation__module.html#ga46619218cf911a9029408a33e63d4c3e',1,'sun_equation_get_sunrise(type_time *sunrise_aux):&#160;sun_equation.c']]],
  ['sun_5fequation_5fget_5fsunset',['sun_equation_get_sunset',['../group__sun__equation__module.html#ga1df1b8069ec0dbf162b7e1f6e5bc7af1',1,'sun_equation_get_sunset(type_time *sunset_aux):&#160;sun_equation.c'],['../group__sun__equation__module.html#ga1df1b8069ec0dbf162b7e1f6e5bc7af1',1,'sun_equation_get_sunset(type_time *sunset_aux):&#160;sun_equation.c']]],
  ['sun_5fequation_5fmodule',['Sun_equation_module',['../group__sun__equation__module.html',1,'']]]
];
