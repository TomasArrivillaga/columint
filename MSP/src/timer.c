

#include <timer.h>

static type_moment current_moment;
static int days_since; 
static int month_days[13]={0,31,28,31,30,31,30,31,31,30,31,30,31};

int new_minute=0;
int new_day=0;
int new_second=0;

void timer_calculate_days_since(type_date date);

void timer_calculate_days_since(type_date date)
{
	days_since=0;
	int i;
	for (i=1;i<date.month;i++)
	{
		days_since+=month_days[i];

	}
	days_since+=date.day;

}

int timer_get_days_since_new_year()
{
	return days_since;

}


int timer_mark_reached(type_moment mark) // no llegue = 0 , estoy = 1, ya pase = 2
{
	if (current_moment.date.year < mark.date.year)
		return 0;
	if (current_moment.date.year > mark.date.year)
		return 2;
	if (current_moment.date.year == mark.date.year)
	{
		if (current_moment.date.month < mark.date.month)
			return 0;
		if (current_moment.date.month > mark.date.month)
			return 2;
		if (current_moment.date.month == mark.date.month)
		{
			if (current_moment.date.day < mark.date.day)
				return 0;
			if (current_moment.date.day > mark.date.day)
				return 2;
			if (current_moment.date.day == mark.date.day)
			{
				if (current_moment.time.hours < mark.time.hours)
					return 0;
				if (current_moment.time.hours > mark.time.hours)
					return 2;
				if (current_moment.time.hours == mark.time.hours)
				{
					if (current_moment.time.minutes < mark.time.minutes)
						return 0;
					if (current_moment.time.minutes > mark.time.minutes)
						return 2;
					if (current_moment.time.minutes == mark.time.minutes)
					{
						if (current_moment.time.seconds < mark.time.seconds)
							return 0;
						if (current_moment.time.seconds > mark.time.seconds)
							return 2;
						if (current_moment.time.seconds == mark.time.seconds)
							return 1;
					}
				}
			}
		}	
	}
	return 0;
}

int timer_minutes_between(type_time ref_time , type_time other_time)
{
    return (other_time.hours-ref_time.hours)*60+(other_time.minutes-ref_time.minutes);
}


int timer_is_leap_year()
{

  return (current_moment).date.year % 4 == 0 ;

}
void timer_set_moment(type_moment min)
{

	current_moment=min;

	month_days[2]= timer_is_leap_year() ? 29 : 28;

	timer_calculate_days_since(current_moment.date);

}

void timer_inc_time(){    
    new_second=1;
    timer_inc_time_generic(&current_moment);
    if (((current_moment).time.seconds==0))
            new_minute = 1;
    if ( (current_moment).time.seconds==0 && (current_moment).time.minutes==0 && (current_moment).time.hours==0)
            new_day = 1;
     
}

void timer_inc_time_generic(type_moment* moment){

	(*moment).time.seconds++;

		if ((*moment).time.seconds==60){
			(*moment).time.seconds=0;
			(*moment).time.minutes++;
			if ((*moment).time.minutes==60){
				(*moment).time.minutes=0;
				(*moment).time.hours++;
				if ((*moment).time.hours==24){
					(*moment).time.hours=0;
					timer_inc_date_generic(moment);
				}
			}
		}
}


void timer_inc_date_generic(type_moment * moment)
{
	int bisiesto =0;
	days_since++;
	
	(*moment).date.day++;
	if ( (*moment).date.month==2)
	{
		bisiesto=((*moment).date.year / 4 == 0);
		if (month_days[(*moment).date.month]==29)
			bisiesto=0;
	}
	
	if ( (*moment).date.day>month_days[(*moment).date.month] + bisiesto )
	{
		(*moment).date.day=1;
		(*moment).date.month++;
		if ( (*moment).date.month>12 )
		{
			(*moment).date.month=1;
			(*moment).date.year++;
		}
	}

}
void timer_inc_date ()
{
    timer_inc_date_generic(&current_moment);
    if (((current_moment).date.month==1) && ((current_moment).date.day == 1))
        month_days[2]= timer_is_leap_year() ? 29 : 28;
}

void timer_get_moment(type_moment* m_out)
{
    *m_out=current_moment;
}

int timer_is_weekend()
{
	int Y,y,c,d,m, weekday;
	if (current_moment.date.month>2)
	{
		m = current_moment.date.month-2;
		Y = current_moment.date.year;
		y = Y % 100;
		c = Y / 100;
	}	
	else
	{
		m = current_moment.date.month+10;
		Y = current_moment.date.year-1;
		y = Y % 100;
		c = Y / 100;
	}
	d = current_moment.date.day;

	weekday = (d+(int)(2.6*m-0.2)+y+(int)(y/4)+(int)(c/4)-2*c)%7;
	return ((weekday == 0) || (weekday == 6));
}


int timer_valid_moment(type_moment moment_in)
{
	int flag = 1;
	int leap_y=0;
	if ((moment_in.date.year < 2000) || (moment_in.date.month < 1) || (moment_in.date.day < 1) || (moment_in.time.hours < 0) || (moment_in.time.minutes <0)|| (moment_in.time.seconds < 0) )
		flag = 0;
	if ((moment_in.date.month>12) || (moment_in.time.hours > 23) || (moment_in.time.minutes > 59) || (moment_in.time.seconds > 59))
		flag = 0;
	if(!(moment_in.date.year%4))
		leap_y=1;
	if (moment_in.date.month!=2)
	{
		if(moment_in.date.day > month_days[moment_in.date.month])
			flag = 0;
	}
	else
	{
		if(moment_in.date.day>28+leap_y)
			flag = 0;
	}
	return flag;
		
}

void timer_time_inc_minutes(type_time* time, int minutes){
    int i=0;
    for(i=0;i<minutes;i++)
    {
        (*time).minutes++;
        if ((*time).minutes==60){
            (*time).minutes=0;
            (*time).hours++;
            if ((*time).hours==24){
                (*time).hours=0;
            }
        }
    }
}

void timer_time_dec_minutes(type_time* time, int minutes){
    int i=0;
    for(i=0;i<minutes;i++)
    {
        if((*time).minutes == 0){
            (*time).minutes=59;
            if((*time).hours == 0){
                (*time).hours =23;
            }else{
                (*time).hours--;
            }
        }else{
            (*time).minutes--;
        }
    }
}














