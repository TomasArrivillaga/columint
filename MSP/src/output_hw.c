#include <output_hw.h>
void pwm_config()
{
    P2DIR |= BIT5; //Set pin 2.5 to the output direction.
    P2SEL0 |= BIT5; //Select pin 2.5 as our PWM output.

    TA0CTL      |= TASSEL_1 + MC_1 + ID_0;      //TASSEL_1 = selecciona ACLK, MC_1 = usa el modo UP.
    TA0CCR0     = 19;                           //periodo de señal (escogimos 20 para poder dimerizar cada 5%) saca una frecuencia de 1638.4 Hz

    TA0CCTL2 |= OUTMOD_7;                       // hace que la señal sea 1 de 0 hasta TA1CCR1, y 0 desde TA1CCR1 hasta TA1CCR0
    TA0CCR2     =0;


    //Set pin 5.4 to the output direction
    P5DIR |= BIT4;
    P5SEL0 &= ~BIT4;
    P5SEL1 &= ~BIT4;
    P5OUT &= ~BIT4; //inicializo en 0
}

void pwm_set_output(float duty_cycle)
{

	if (duty_cycle<0 || duty_cycle>=0.1){
		P5OUT |= BIT4; //Enciendo la luminaria. El negativo es para el caso de que no hay eventos en modo Plan Horario
	}
	else{
        P5OUT &= ~BIT4; //Apago la luminaria
	}

	TA0CCR2=round((duty_cycle/100)*(TA0CCR0+1));

}
