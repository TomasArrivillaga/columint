

#include <UART_hw.h>


char TxBuff[MAX_SIZE_MSG];
char RxBuff[MAX_SIZE_MSG];
int RxMessageComplete=0;
int TxMessageComplete=1;
unsigned int RxHead, TxHead;

void UART_hw_config(void)
{

    const eUSCI_UART_Config uartConfig =
    {
            EUSCI_A_UART_CLOCKSOURCE_ACLK ,          // ACLK Clock Source
            3,                                      // BRDIV = 78
            0,                                       // UCxBRF = 2
            0x92,                                    // UCxBRS = 0x92
            EUSCI_A_UART_NO_PARITY,                  // No Parity
            EUSCI_A_UART_LSB_FIRST,                  // LSB First
            EUSCI_A_UART_ONE_STOP_BIT,               // One stop bit
            EUSCI_A_UART_MODE,                       // UART mode
            EUSCI_A_UART_LOW_FREQUENCY_BAUDRATE_GENERATION  // Oversampling
    };

    /* Selecting P1.2 and P1.3 in UART mode */
    MAP_GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P1,
    GPIO_PIN2 | GPIO_PIN3, GPIO_PRIMARY_MODULE_FUNCTION);

    /* Configuring UART Module */
    MAP_UART_initModule(EUSCI_A0_BASE, &uartConfig);

    /* Enable UART module */
    MAP_UART_enableModule(EUSCI_A0_BASE);

    UART_enableInterrupt(EUSCI_A0_BASE,EUSCI_A_UART_RECEIVE_INTERRUPT );


}

void handle_Rx(char* extBuff)
{
    do
    {
        extBuff[RxHead] = RxBuff[RxHead];
        RxHead++;
    } while (RxBuff[RxHead-1] != '\r');
    RxHead = 0;
}

/*********************
*
*
*    TRANSMISION
*
*
***********************/

void beginTx(char* extBuff){
    int n=0;

    for (n=0; n<MAX_SIZE_MSG; n++){
    TxBuff[n]=extBuff[n];
    }
    TxMessageComplete=0;
    UART_hw_first_bit_TX(TxBuff[0]) ;

}

void UART_hw_first_bit_TX(char a)
{
    UCA0TXBUF=a;
    UCA0IE  |= UCTXIE;
}

void EUSCIA0_IRQHandler(){

    int status=MAP_UART_getEnabledInterruptStatus(EUSCI_A0_BASE);

    if(MAP_UART_getEnabledInterruptStatus(EUSCI_A0_BASE) & EUSCI_A_UART_RECEIVE_INTERRUPT_FLAG)
    {
    RxBuff[RxHead] = UCA0RXBUF;
    if (RxBuff[RxHead] == '\r' || (RxHead >= MAX_SIZE_MSG))
    {
        RxMessageComplete = 1;
        RxHead = 0;
        __low_power_mode_off_on_exit();


    }
    else
        RxHead++;
    }

    if((MAP_UART_getEnabledInterruptStatus(EUSCI_A0_BASE) & EUSCI_A_UART_TRANSMIT_INTERRUPT_FLAG))
    {
    if ((TxHead < MAX_SIZE_MSG) && !(TxBuff[TxHead-1]=='\r')) {
         UCA0TXBUF=TxBuff[TxHead];
         TxHead++;
         TxMessageComplete=0;
     }else{
         TxHead=1;
         UCA0IE &= ~UCTXIE;
         EUSCI_A0->IFG &= ~EUSCI_A_IFG_TXIFG;
         TxMessageComplete=1;
     }
    }

}



