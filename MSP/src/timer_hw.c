/**********************************************************
*
* @file timer_hw.c
*
* Proyecto lab2-previo
* Modulo timer_hw
*
* Este modulo agrupa las funciones void que configuran el periferico Timer_A
*	y tambien la rutina de interrupcion atada a este periferico.
*
* timer_hw.c
* @version 1.0
* @author  	Emilio Albarrracin 	<ealbarracion2911@gmail.com>
* @author	Tomás Arrivillaga	<tomas92@ymail.com>
* @author	Felipe Morán		<felipemoran73@gmail.com>
*
* @version 1.1
* @author  	Emilio Albarrracin 	<ealbarracion2911@gmail.com>
* @author	Tomás Arrivillaga	<tomas92@ymail.com>
* @author	Felipe Morán		<felipemoran73@gmail.com>

* Se corrige el uso de |= en vez de = en la implementacion configuración.
* @date 22 Marzo 2019
*
* @version 2.0
* @author  	Emilio Albarrracin 	<ealbarracion2911@gmail.com>
* @author	Tomás Arrivillaga	<tomas92@ymail.com>
* @author	Felipe Morán		<felipemoran73@gmail.com>
*
* se añade medicion de temperatura para lab3 sekere
**********************************************************/

//----------------------------------------------------------
/** Configura los parametros del Timer_A para una interrupcion de 1s
	usando el ACLK y el cristal XT1
 */

//----------------------------------------------------------
#include <timer_hw.h>

void timer_hw_config_timer_A(void){
	//BCSCTL1 |= DIVA_0;						
    CS->KEY   = CS_KEY_VAL ;                                          // Unlock CS module for register access
    CS->CTL0 |= CS_CTL0_DCOEN;
    CS->CTL2 |= CS_CTL2_LFXT_EN;                                    // habilitar timer de cristal.
    CS->CTL1 |= CS_CTL1_SELA_0;                                     // ACLK USANDO LFXT
    CS->CLKEN = CS_CLKEN_ACLK_EN + CS_CLKEN_SMCLK_EN;
    CS->CTL0 |= CS_CTL0_DCOEN;

    CS->KEY = 0;                                                    // Lock CS module from unintended accesses

	TIMER_A1->CTL	|= TIMER_A_CTL_TASSEL_1 + TIMER_A_CTL_MC_1 + TIMER_A_CTL_ID_0;		//TASSEL_1 = selecciona ACLK, MC_1 = usa el modo UP.
	TIMER_A1->CCR[0] 	= 32767;						                                //Maximo necesario para la interrupcion cada 1s.
	TIMER_A1->CCTL[0]	|= TIMER_A_CCTLN_CCIE  ;						                   //habilita la interrupcion.
}

//Necesario para la interrupcion del Timer_A
void TA1_0_IRQHandler (void)
{
    timer_inc_time();
//    P1OUT ^=BIT0;
    TIMER_A1->CCTL[0] &= ~CCIFG;
    __low_power_mode_off_on_exit();
}
