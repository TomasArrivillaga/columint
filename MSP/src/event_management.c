
#define MINS_IN_A_DAY 1440 //cantidad de minutos en un dia

#include <event_management.h>

const type_event null_event = {{-1,-1,-1}, 0};

static type_event weekend_events [MAX_SP_EVENTS];
static type_event week_events [MAX_SP_EVENTS];
static type_event sun_events[2];

int event_cmp(type_event a,type_event b);

void event_new_sun_events()
{
    sun_equation_get_sunrise(&(sun_events[0].event_time));
    sun_events[0].dim_lvl = 0;
    sun_equation_get_sunset(&(sun_events[1].event_time));
    sun_events[1].dim_lvl = 100;
    next_event_index_AC=event_calculate_next_index(ASTRO_CLK);
}

void event_init()
{
    int i;
    //event_must_be_handled=1;
    //SET SUNRISE EVENTS
    event_new_sun_events();


    //SET DEFAULT SCHEDULE PLAN

    for (i = 0 ; i<MAX_SP_EVENTS ; i++)
    {
        if (i<2)
        {
            week_events[i] = sun_events[i];
            weekend_events[i] = sun_events[i];    
        } 
        else
        {
            week_events[i] = null_event;
            weekend_events[i] = null_event;
        }
        
    }

    //INDICES ACTUALES
    next_event_index_AC = event_calculate_next_index(ASTRO_CLK);

    next_event_index_SP = event_calculate_next_index(SCHEDULE_PLAN);

}

void event_save_index(type_mode mode)
{
    if (mode == ASTRO_CLK)
    next_event_index_AC = event_calculate_next_index(mode);
    if (mode == SCHEDULE_PLAN)
    next_event_index_SP = event_calculate_next_index(mode);
}

void event_remove (int day_timer_is_weekend, int index)
{
    if (day_timer_is_weekend)
    {
        if (event_cmp(null_event,weekend_events[index]))
        {
            return;
        }
        else
        {
            weekend_events[index] = null_event;
            return;
        }


    }
    else
    {
        if (event_cmp(null_event,week_events[index]))
        {
            return;
        }
        else
        {
            week_events[index] = null_event;
            return;
        }
    }
}

void event_reset (int day_timer_is_weekend)
{   int i;

    for (i = 0; i<MAX_SP_EVENTS; i++)
    {
        event_remove(day_timer_is_weekend, i);
    }

}

int event_time_cmp(type_event a,type_event b)
{
    int result=1;

    result = result && (a.event_time.hours==b.event_time.hours);
    result = result && (a.event_time.minutes==b.event_time.minutes);

    return(result);

}

int event_cmp(type_event a,type_event b)
{
    int result = 1;
    result = event_time_cmp(a,b);
    result = result && (a.dim_lvl==b.dim_lvl);

    return(result);
}

void event_add (int day_timer_is_weekend, type_event new_event) 
{
    int n = 0;
    int i = 0;
    int error=0;
    
    if (day_timer_is_weekend)
    {
        while (error==0 && n<MAX_SP_EVENTS)
        {
            error=(event_time_cmp(weekend_events[n],new_event));
            n++;
        }
        if (error)
            return;

        while ((i<MAX_SP_EVENTS) && !(event_cmp(weekend_events[i],null_event)))
        {
            i++;
        }
        if (i == MAX_SP_EVENTS)
            return;
        else
        {
            weekend_events[i] = new_event;
            next_event_index_SP = event_calculate_next_index(SCHEDULE_PLAN);
            return;
        }
    }
    else
    {
        while (error==0 && n<MAX_SP_EVENTS)
        {
            error=(event_time_cmp(week_events[n],new_event));
            n++;
        }
        if (error)
            return;

        while ((i<MAX_SP_EVENTS) && !(event_cmp(week_events[i],null_event)))
        {
            i++;
        }
        if (i == MAX_SP_EVENTS)
            return;
        else
        {
            week_events[i] = new_event;
            next_event_index_SP = event_calculate_next_index(SCHEDULE_PLAN);
            return;
        }
    }
}


int event_reached(type_mode mode)
{ 
    type_moment current_moment;
    timer_get_moment(&current_moment);
	switch (mode)
	{
		case (ASTRO_CLK):
			{
				return (sun_events[next_event_index_AC].event_time.hours == current_moment.time.hours) && (sun_events[next_event_index_AC].event_time.minutes == current_moment.time.minutes);
			}
		case (SCHEDULE_PLAN):
		{
			if (timer_is_weekend())
				return (weekend_events[next_event_index_SP].event_time.hours == current_moment.time.hours) && (weekend_events[next_event_index_SP].event_time.minutes == current_moment.time.minutes);
			else
				return (week_events[next_event_index_SP].event_time.hours == current_moment.time.hours) && (week_events[next_event_index_SP].event_time.minutes == current_moment.time.minutes);

		}	
	} 
	return 0;
}

int event_get_dim_level(type_mode mode, int index)
{
    int dim_level;
    if (mode == ASTRO_CLK)
    {
        dim_level=sun_events[index].dim_lvl;

    }
    if (mode == SCHEDULE_PLAN)
    {
        if (timer_is_weekend())
            dim_level=weekend_events[index].dim_lvl;
        else
            dim_level=week_events[index].dim_lvl;

    }

    return(dim_level);

}

int event_get_next_dim_level(type_mode mode)
{
    int aux;

    if(mode == ASTRO_CLK)
    {
        aux = event_get_dim_level(mode, next_event_index_AC);
        next_event_index_AC = event_calculate_next_index(ASTRO_CLK);
    }
    if(mode == SCHEDULE_PLAN)
    {
        aux = event_get_dim_level(mode, next_event_index_SP);
        next_event_index_SP = event_calculate_next_index(SCHEDULE_PLAN);
    }
    return(aux);
}
int event_calculate_next_index (type_mode mode_in)
{
    type_moment m_aux;
    timer_get_moment(&m_aux);
    int index, i;
    /****************MODO ASTRO CLK****************/

    if (mode_in == ASTRO_CLK)
    {
        type_moment sunrise_moment, sunset_moment; //Usamos timer_mark_reached, la cual tiene como input un type_moment, asi que hay que generar moments de los eventos
        
        sunrise_moment = m_aux; //Para tener la fecha actual
        //Ahora pongo hora y minutos del evento
        sunrise_moment.time.hours = sun_events[0].event_time.hours;
        sunrise_moment.time.minutes = sun_events[0].event_time.minutes;

        //Idem lo anterior pero para sunset
        sunset_moment = sunrise_moment;
        sunset_moment.time.hours = sun_events[1].event_time.hours;
        sunset_moment.time.minutes = sun_events[1].event_time.minutes;        

        if (timer_mark_reached(sunset_moment))
            index = 2;
        else if (timer_mark_reached(sunrise_moment))
            index = 1;
        else 
            index = 0;
    }    

    /****************MODO SCH_PLAN****************/    
    if (mode_in == SCHEDULE_PLAN)
    {
        int mins_remaining = MINS_IN_A_DAY;
        int mins_aux;
        for (i=0 ; i<MAX_SP_EVENTS ; i++)
        {
            if (timer_is_weekend())
            {
                mins_aux = timer_minutes_between((m_aux.time) , (weekend_events[i].event_time));
                if ((mins_aux > 0) && (mins_aux < mins_remaining))
                {
                    mins_remaining = mins_aux;
                    index = i; 
                }        
            }
            else
            {
                mins_aux = timer_minutes_between((m_aux.time) , (week_events[i].event_time));
                if ((mins_aux > 0) && (mins_aux < mins_remaining))
                {
                    mins_remaining = mins_aux;
                    index = i; 
                }    
            }
        }
        if (mins_remaining == MINS_IN_A_DAY) // No quedan eventos en el dia (no entro en ningun if)
            return MAX_SP_EVENTS;
    }
    return index;
}

int event_calculate_current_index (type_mode mode_in)
{
    type_moment m_aux;
    timer_get_moment(&m_aux);
    int index, i;
    /****************MODO ASTRO CLK****************/

    if (mode_in == ASTRO_CLK)
    {
        type_moment sunrise_moment, sunset_moment; //Usamos mark_reached, la cual tiene como input un type_moment, asi que hay que generar moments de los eventos
        
        sunrise_moment = m_aux; //Para tener la fecha actual
        //Ahora pongo hora y minutos del evento
        sunrise_moment.time.hours = sun_events[0].event_time.hours;
        sunrise_moment.time.minutes = sun_events[0].event_time.minutes;

        //Idem lo anterior pero para sunset
        sunset_moment = sunrise_moment;
        sunset_moment.time.hours = sun_events[1].event_time.hours;
        sunset_moment.time.minutes = sun_events[1].event_time.minutes;        

        if (timer_mark_reached(sunset_moment))
            index = 1;
        else if (timer_mark_reached(sunrise_moment))
            index = 0;
        else 
            index = 1;
    }    

    /****************MODO SCH_PLAN****************/    
    if (mode_in == SCHEDULE_PLAN)
    {
        int mins_passed = MINS_IN_A_DAY;
        int mins_aux;
        for (i=0 ; i<MAX_SP_EVENTS ; i++)
        {
            if (timer_is_weekend())
            {
                mins_aux = timer_minutes_between((weekend_events[i].event_time) , (m_aux.time));
                if ((mins_aux > 0) && (mins_aux < mins_passed  ))
                {
                    mins_passed = mins_aux;
                    index = i; 
                }        
            }
            else
            {
                mins_aux = timer_minutes_between((week_events[i].event_time) , (m_aux.time));
                if ((mins_aux > 0) && (mins_aux < mins_passed ))
                {
                    mins_passed = mins_aux;
                    index = i; 
                }    
            }
        }
        if (mins_passed  == MINS_IN_A_DAY) // No quedan han pasado eventos en el dia (no entro en ningun if)
        {	
        	type_time hora_cero = {0,0,0};
        	mins_passed = 0; //uso mins_passed para hallar el evento que dista mas de la hora cero (el evento mas tardio)
        	if (timer_is_weekend())
        	{
        		for (i=0 ; i<MAX_SP_EVENTS ; i++)
	        	{
	        		mins_aux = timer_minutes_between(hora_cero,weekend_events[i].event_time);
	        		if (mins_aux > mins_passed)
	        		{
	        			mins_passed = mins_aux;
	        			index = i;	
	        		}
	        	}	
        	}
        	else
        	{
        		for (i=0 ; i<MAX_SP_EVENTS ; i++)
	        	{
	        		mins_aux = timer_minutes_between(hora_cero,week_events[i].event_time);
	        		if (mins_aux > mins_passed)
	        		{
	        			mins_passed = mins_aux;
	        			index = i;	
	        		}
	        	}	
        	}	
        	
        	return index;

        }    
            
    }
    return index;
}


type_event event_create(int h, int m, int d)
{
    type_event new_event;

    new_event.event_time.hours=h;
    new_event.event_time.minutes=m;
    new_event.event_time.seconds=0;

    new_event.dim_lvl=d;

    return (new_event);
}

void event_get(type_event* event,int timer_is_weekend, int index)
{
    if(timer_is_weekend)
    {
    event->event_time.hours=weekend_events[index].event_time.hours;
    event->event_time.minutes=weekend_events[index].event_time.minutes;
    event->dim_lvl=weekend_events[index].dim_lvl;
    }
    else
    {
        event->event_time.hours=week_events[index].event_time.hours;
        event->event_time.minutes=week_events[index].event_time.minutes;
        event->dim_lvl=week_events[index].dim_lvl;
    }

}

int event_is_day()
{
    return (event_calculate_next_index(ASTRO_CLK)==1);
}

