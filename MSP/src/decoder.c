#include <decoder.h>


uint8_t decoder_decode_RX(char* buffer)
{
        int i, secs_since_midnite, mins_since_midnite, years_since_2000, days_since_ny, dim_lvl, flag_use_phc;
        static float volt, curr, actpwr, apppwr, reapwr, pfactor;
        int aux_acs71020;
        char command = (buffer[0])>>3; //me quedo con los 5 bits de la izquierda
        int ID_list[32];
        int mins_since_midnite_list[32];
        int dim_lvl_list[32];
        int cmd_response_size=0; //tamano en bytes de la respuesta
        type_mode mode;
        switch (command)
        {
                case SET_ON:
                {
                        control_do_on();
                        control_do_set_mode(DIRECT);
                        break;
                }
                case SET_OFF:
                {
                        control_do_off();
                        control_do_set_mode(DIRECT);
                        break;
                }
                case SET_DIM:
                {
                        control_do_dim ((int)buffer[1]); //este casteo funciona si el valor viene en hex o uint8_t?
                        control_do_set_mode(DIRECT);
                        break;
                }
                case ADD_EVENT_WE:
                {
                        mins_since_midnite = (8)*(int)buffer[1] + (int)(buffer[2]>>5);
                        dim_lvl = 5*(int)(buffer[2] & DIM_LVL_MSK);
                        control_do_add_event(WE, mins_since_midnite, dim_lvl);
                        break;        
                }
                case ADD_EVENT_WD:
                {
                        mins_since_midnite = (8)*(int)buffer[1] + (int)(buffer[2]>>5);
                        dim_lvl = 5*(int)(buffer[2] & DIM_LVL_MSK);
                        control_do_add_event(WD, mins_since_midnite, dim_lvl);
                        break;
                }
                case RES_EVENT_WE:
                {
                        control_do_res_event(WE);
                        break;
                }
                case RES_EVENT_WD:
                {
                        control_do_res_event(WD);
                        break;
                }
                case DIR_EVENT_WE:
                {
                        //get lists
                        control_do_dir_event(ID_list, mins_since_midnite_list, dim_lvl_list, WE);
                        //make cmd_response bytelist to send
                        i = 0;
                        while (ID_list[i]>=0) 
                        {
                                buffer[3*i] = (char) (mins_since_midnite_list[i] / 8);
                                buffer[3*i + 1] = ((char)(mins_since_midnite_list[i] % 8) << 5) | (char) ID_list[i] ;
                                buffer[3*i + 2] = (char) dim_lvl_list[i];
                                i++;
                        }
                        cmd_response_size = 3 * (i);
                        buffer[cmd_response_size]=1;
                        lora_begin_tx = 1;
                        break;
                }
                case DIR_EVENT_WD:
                {
                        //get lists
                        control_do_dir_event(ID_list, mins_since_midnite_list, dim_lvl_list, WD);
                        //make cmd_response bytelist to send
                        i = 0;
                        while (ID_list[i]>=0) 
                        {
                                buffer[3*i] = (char) (mins_since_midnite_list[i] / 8);
                                buffer[3*i + 1] = ((char)(mins_since_midnite_list[i] % 8) << 5) | (char) ID_list[i] ;
                                buffer[3*i + 2] = (char) dim_lvl_list[i];
                                i++;
                        }
                        cmd_response_size = 3 * (i);
                        buffer[cmd_response_size]=6;

                        lora_begin_tx = 1;
                        break;
                }
                case DIR_EVENT_SE:
                {
                        //get lists
                        control_do_dir_event(ID_list, mins_since_midnite_list, dim_lvl_list, SE);
                        //make cmd_response bytelist to send
                        i = 0;
                        while (ID_list[i]>=0) 
                        {
                                buffer[3*i] = (char) (mins_since_midnite_list[i] / 8);
                                buffer[3*i + 1] = ((char)(mins_since_midnite_list[i] % 8) << 5) | (char) ID_list[i] ;
                                buffer[3*i + 2] = (char) dim_lvl_list[i];
                                i++;
                        }
                        cmd_response_size = 3 * (i);
                        buffer[cmd_response_size]=11;
                        lora_begin_tx = 1;
                        break;
                }
                case DEL_EVENT_WE:
                {
                        control_do_del_event(WE, (int) buffer[1]);
                        break;
                }
                case DEL_EVENT_WD:
                {
                        control_do_del_event(WD, (int) buffer[1]);
                        break;
                }

                case SET_MODE:
                {
                        control_do_set_mode((type_mode)(buffer[0])&(~CMD_MSK)); //me quedo con los 2 LSB
                        break;
                }
                case SET_MOMENT:
                {
                        secs_since_midnite = (512)*(int) buffer[1] + 2*(int) buffer[2]+(int) (buffer[3]>>7);
                        years_since_2000 = (buffer[3] & YRS_MSK) >> 1;
                        days_since_ny = (256)*(int)(0x1 & buffer[3]) + (int) buffer[4];
                        control_do_set_moment(years_since_2000, days_since_ny, secs_since_midnite);
                        break;
                }
                case USE_PHC_ON:
                {
                        control_do_use_photocell(PHC_ON);        
                        break;
                }
                case USE_PHC_OFF:
                {
                        control_do_use_photocell(PHC_OFF);        
                        break;
                }
                case GET_MOMENT:
                {
                        control_do_get_moment(&secs_since_midnite,  &days_since_ny, &years_since_2000);
                        buffer[0] = (char)(secs_since_midnite / (512));
                        buffer[1] = (char)((secs_since_midnite % (512))/2);
                        buffer[2] = ((char)(secs_since_midnite % 2) << 7) | ((char)years_since_2000 << 1) | ((char)(days_since_ny / (256)));
                        buffer[3] = (char)(days_since_ny % (256));
                        cmd_response_size = 4;
                        buffer[cmd_response_size]=21;
                        lora_begin_tx = 1;
                        break;
                }
                case GET_MODE:
                {
                        control_do_get_mode(&mode, &flag_use_phc, &dim_lvl);
                        buffer[0] = (char) dim_lvl;
                        buffer[1] = (char)mode | ((char)flag_use_phc << 2);
                        cmd_response_size = 2;
                        buffer[cmd_response_size]=20;
                        lora_begin_tx = 1;        
                        break;
                }
                case GET_VOLT:
                {
                		control_do_get_volt(&volt);
                		aux_acs71020=(int)(volt*10);
                		buffer[0]=(char)(aux_acs71020/256);
                		buffer[1]=(char)(aux_acs71020%256);
                        cmd_response_size = 2;
                        buffer[cmd_response_size]=22;
                        lora_begin_tx = 1;
                        break;
                }
                case GET_CURR:
                {
                		control_do_get_curr(&curr);
                		aux_acs71020=(int)(curr*10000);
						buffer[0]=(char)(aux_acs71020/256);
						buffer[1]=(char)(aux_acs71020%256);
                        cmd_response_size = 2;
                        buffer[cmd_response_size]=23;
                        lora_begin_tx = 1;
                        break;
                }
                case GET_ACTPWR:
                {
                		control_do_get_actpwr(&actpwr);
                		aux_acs71020=(int)(actpwr*10);
						buffer[0]=(char)(aux_acs71020/256);
						buffer[1]=(char)(aux_acs71020%256);
						if (actpwr<0){
							buffer[1]|=0x80;
						}
						else{
							buffer[1]&=0x7F;
						}
                        cmd_response_size = 2;
                        buffer[cmd_response_size]=24;
                        lora_begin_tx = 1;
                        break;        
                }
                case GET_REAPWR:
                {
                		control_do_get_reapwr(&reapwr);
                		aux_acs71020=(int)(reapwr*10);
						buffer[0]=(char)(aux_acs71020/256);
						buffer[1]=(char)(aux_acs71020%256);
						if (reapwr<0){
							buffer[1]|=0x80;
						}
						else{
							buffer[1]&=0x7F;
						}
                        cmd_response_size = 2;
                        buffer[cmd_response_size]=25;
                        lora_begin_tx = 1;
                        break;        
                }
                case GET_APPPWR:
                {
                		control_do_get_apppwr(&apppwr);
                		aux_acs71020=(int)(apppwr*10);
						buffer[0]=(char)(aux_acs71020/256);
						buffer[1]=(char)(aux_acs71020%256);
						if (apppwr<0){
							buffer[1]|=0x80;
						}
						else{
							buffer[1]&=0x7F;
						}
                        cmd_response_size = 2;
                        buffer[cmd_response_size]=26;
                        lora_begin_tx = 1;
                        break;        
                }
                case GET_PFACTOR:
                {
                		control_do_get_pfactor(&pfactor);
                		aux_acs71020=(int)(pfactor*100);
						buffer[0]=(char)(aux_acs71020/256);
						buffer[1]=(char)(aux_acs71020%256);
						if (pfactor<0){
							buffer[1]|=0x80;
						}
						else{
							buffer[1]&=0x7F;
						}
                        cmd_response_size = 2;
                        buffer[cmd_response_size]=27;
                        lora_begin_tx = 1;
                        break;        
                }
        }
        return(cmd_response_size);
}
