/*
 * acs71020_hw.c
 *
 *  Created on: 28 jul. 2020
 *      Author: Emilio
 */

#include <driverlib.h>
#include <acs71020_hw.h>

uint8_t acs_TxData[NUM_OF_TX_BYTES];
uint8_t acs_RxData[NUM_OF_REC_BYTES];

static volatile uint32_t xferIndex;
volatile bool stopSent;
static uint8_t TXByteCtr;
static uint8_t acs_TxData_local[NUM_OF_TX_BYTES];
static bool toRead;

/* I2C Master Configuration Parameter */
eUSCI_I2C_MasterConfig i2cConfig =
{
        EUSCI_B_I2C_CLOCKSOURCE_ACLK,          // ACLK Clock Source
        3000000,                                // SMCLK = 3MHz
        EUSCI_B_I2C_SET_DATA_RATE_100KBPS,      // Desired I2C Clock of 100khz
        0,                                      // No byte counter threshold
        EUSCI_B_I2C_NO_AUTO_STOP                // No Autostop
};

void acs71020_config(){

    /* Select Port 1 for I2C - Set Pin 6, 7 to input Primary Module Function,
     *   (UCB2SIMO/UCB2SDA, UCB2SOMI/UCB2SCL).
     */
    MAP_GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P3,
            GPIO_PIN6 + GPIO_PIN7, GPIO_PRIMARY_MODULE_FUNCTION);
    stopSent = false;
    uint8_t i;
    for (i=0;i<NUM_OF_REC_BYTES;i++)
    {
        acs_RxData[i]=0;
    }

    /* Initializing I2C Master to SMCLK at 100khz with no autostop */
    MAP_I2C_initMaster(EUSCI_B2_BASE, &i2cConfig);

    /* Specify slave address */
    MAP_I2C_setSlaveAddress(EUSCI_B2_BASE, SLAVE_ADDRESS);

    /* Set Master in transmit mode */
    MAP_I2C_setMode(EUSCI_B2_BASE, EUSCI_B_I2C_TRANSMIT_MODE);

    /* Enable I2C Module to start operations */
    MAP_I2C_enableModule(EUSCI_B2_BASE);

    /* Enable and clear the interrupt flag */
    MAP_I2C_clearInterruptFlag(EUSCI_B2_BASE,
            EUSCI_B_I2C_TRANSMIT_INTERRUPT0 + EUSCI_B_I2C_RECEIVE_INTERRUPT0);
    //Enable master Transmit interrupt
    MAP_I2C_enableInterrupt(EUSCI_B2_BASE, EUSCI_B_I2C_TRANSMIT_INTERRUPT0);
    MAP_Interrupt_enableSleepOnIsrExit();
    MAP_Interrupt_enableInterrupt(INT_EUSCIB2);	//habilita interrupciones para comunicaci�n i2c con acs71020

	acs_TxData_local[0] = 0x4f;
	acs_TxData_local[1] = 0x70;
	acs_TxData_local[2] = 0x65;
	acs_TxData_local[3] = 0x6e;
    acs71020_write(0x2f, acs_TxData_local);		//Escribo customer code para poder escribir registros

}

void acs71020_write (uint8_t register_id, uint8_t* data){

	toRead=0;
	TXByteCtr=NUM_OF_TX_BYTES-1;  //1 menos de la cantidad a mandar
	acs_TxData[TXByteCtr]=register_id;

    uint8_t i;
    for (i=0;i<NUM_OF_TX_BYTES-1;i++)
    {
        acs_TxData[i]=data[i];						//OJO: El que se escriba en acs_TxData[3] ser� el menos significativo!!!
    }

	/* Making sure the last transaction has been completely sent out */
	while (MAP_I2C_masterIsStopSent(EUSCI_B2_BASE) == EUSCI_B_I2C_SENDING_STOP);

	/* Send start and the first byte of the transmit buffer */
	MAP_I2C_masterSendMultiByteStart(EUSCI_B2_BASE, acs_TxData[TXByteCtr]);

	/* While the stop condition hasn't been sent out...*/
	while(!stopSent)
	{
		//MAP_PCM_gotoLPM0InterruptSafe();   Si se usa se rompen interrupciones
	}
	stopSent = false;

}


void acs71020_read (uint8_t register_id){

	toRead=1;
	acs_TxData[0] = register_id;

	/* Making sure the last transaction has been completely sent out */
	while (MAP_I2C_masterIsStopSent(EUSCI_B2_BASE) == EUSCI_B_I2C_SENDING_STOP);

	/* Send start and the first byte of the transmit buffer */
	MAP_I2C_masterSendMultiByteStart(EUSCI_B2_BASE, acs_TxData[TXByteCtr]);

	/* While the stop condition hasn't been sent out...*/
	while(!stopSent)
	{
		//MAP_PCM_gotoLPM0InterruptSafe();   Si se usa se rompen interrupciones
	}
	stopSent = false;

}


/*******************************************************************************
 * eUSCIB2 ISR. The repeated start and transmit/receive operations happen
 * within this ISR.
 *******************************************************************************/
void EUSCIB2_IRQHandler(void)
{
    uint_fast16_t status;

    status = MAP_I2C_getEnabledInterruptStatus(EUSCI_B2_BASE);
    MAP_I2C_clearInterruptFlag(EUSCI_B2_BASE, status);

    /* If we reached the transmit interrupt, it means we are at index 1 of
     * the transmit buffer. When doing a repeated start, before we reach the
     * last byte we will need to change the mode to receive mode, set the start
     * condition send bit, and then load the final byte into the TXBUF.
     */
    if (status & EUSCI_B_I2C_TRANSMIT_INTERRUPT0)
    {
    	/*Check if it is a write in order to read a register*/
    	if (toRead){
    		MAP_I2C_masterSendMultiByteStop(EUSCI_B2_BASE);
			MAP_I2C_disableInterrupt(EUSCI_B2_BASE, EUSCI_B_I2C_TRANSMIT_INTERRUPT0);
			MAP_I2C_setMode(EUSCI_B2_BASE, EUSCI_B_I2C_RECEIVE_MODE);
			xferIndex = 0;
			MAP_I2C_masterReceiveStart(EUSCI_B2_BASE);
			MAP_I2C_enableInterrupt(EUSCI_B2_BASE, EUSCI_B_I2C_RECEIVE_INTERRUPT0);
    	}

    	/*It is a write only. Check the TX byte counter*/
    	else if (TXByteCtr){
    		MAP_I2C_masterSendMultiByteNext(EUSCI_B2_BASE, acs_TxData[TXByteCtr-1]);
    		TXByteCtr--;
    	}
    	/*All data has been sent*/
    	else{
    		MAP_I2C_masterSendMultiByteStop(EUSCI_B2_BASE);
    		stopSent = true;
    		MAP_Interrupt_disableSleepOnIsrExit();
    	}

    }

    /* Receives bytes into the receive buffer. If we have received all bytes,
     * send a STOP condition */
    if (status & EUSCI_B_I2C_RECEIVE_INTERRUPT0)
    {
        if(xferIndex == NUM_OF_REC_BYTES - 2)
        {
            MAP_I2C_masterReceiveMultiByteStop(EUSCI_B2_BASE);
            acs_RxData[xferIndex++] = MAP_I2C_masterReceiveMultiByteNext(EUSCI_B2_BASE);
        }
        else if(xferIndex == NUM_OF_REC_BYTES - 1)
        {
            acs_RxData[xferIndex++] = MAP_I2C_masterReceiveMultiByteNext(EUSCI_B2_BASE);
            MAP_I2C_disableInterrupt(EUSCI_B2_BASE, EUSCI_B_I2C_RECEIVE_INTERRUPT0);
            MAP_I2C_setMode(EUSCI_B2_BASE, EUSCI_B_I2C_TRANSMIT_MODE);
            xferIndex = 0;
            stopSent = true;
            MAP_I2C_enableInterrupt(EUSCI_B2_BASE, EUSCI_B_I2C_TRANSMIT_INTERRUPT0);
            //MAP_Interrupt_disableSleepOnIsrExit();
        }
        else
        {
            acs_RxData[xferIndex++] = MAP_I2C_masterReceiveMultiByteNext(EUSCI_B2_BASE);
        }

    }
}
