

#include <photocell.h>
float phcAverage=0;

void phc_CheckMeasure(){
    uint_fast16_t analogValue;
    analogValue = phc_GetLastValue();
    phcAverage=phcAverage + (analogValue/60);
    phcTriggerMeasure();
}

uint8_t phc_CheckAverage(){
    uint8_t result;
    if (phcAverage <= LOWERTHRESHOLD){
        result = LOW_AVG;
    }else if(phcAverage>=UPPERTHRESHOLD){
        result = HIGH_AVG;
    }else{
        result = MID_AVG;
    }
    phcAverage=0;
    return(result);
}
