#include <control.h>

#define WD                  0
#define WE                  1
#define SE                  2

extern int phc_turn_on,phc_turn_off,send_it,new_day,TxMessageComplete;
int flag_use_photocell=0;
static int month_days[13]={0,31,28,31,30,31,30,31,31,30,31,30,31};

type_mode current_mode=ASTRO_CLK;
void control_take_action()
{
   uint8_t avg_lvl;
   switch (current_mode)
   {
       case DIRECT:
           break;

       case ASTRO_CLK:
           if( output_get_light_level()!=OFF && event_is_day() && !flag_use_photocell)
               output_set_light_level(OFF);
           if( output_get_light_level()!=ON && !event_is_day())
               output_set_light_level(ON);
           if (flag_use_photocell && event_is_day())
               goto PHC_IS_BEING_USED;

           break;

       case SCHEDULE_PLAN:
           if (event_reached(current_mode))
               output_set_light_level(event_get_next_dim_level(current_mode));

           if (flag_use_photocell && event_is_day())
               goto PHC_IS_BEING_USED;

           break;

       case PHOTOCELL:
           PHC_IS_BEING_USED:
           avg_lvl = phc_CheckAverage();
           if(avg_lvl == LOW_AVG){
               output_set_light_level(ON);
           }else if(avg_lvl == HIGH_AVG){
               output_set_light_level(OFF);
           }
           break;
   }

}

void control_do_set_moment(int years_since_2000, int days_since_ny, int secs_since_midnite)
{
    int y, m, d, h, minu, s, aux;
    type_moment moment;

    y = 2000 + years_since_2000;
    m = 1;
    aux = days_since_ny;
    while (aux > 0)
    {
        aux -= month_days[m];
        if ((m == 2) && (y % 4 == 0))  // si es leap_year
            aux --;
        if (aux > 0)
            m++;
    }
    d = aux + month_days[m-1]; // sumo lo que reste "de sobra" en el while
    if (!((m > 2) && (y % 4 == 0)))  // si es leap_year le reste un dia extra, compenso
        d--;
    h = secs_since_midnite / 3600;
    minu = (secs_since_midnite % 3600) / 60;
    s = (secs_since_midnite % 3600) % 60;

    moment.date.year=y;
    moment.date.month=m;
    moment.date.day=d;
    moment.time.hours=h;
    moment.time.minutes=minu;
    moment.time.seconds=s;

    if (timer_valid_moment(moment))
    {
        timer_set_moment(moment);
        sun_equation_calc_sun_events(-32.5228,-55.7658,-3);
        event_save_index(SCHEDULE_PLAN);
        new_day=1;
    }
}

void control_do_get_moment(int* secs_since_midnite_p, int* days_since_ny_p, int* years_since_2000_p)
{
    int i, aux_days;
    type_moment aux_moment;

    timer_get_moment(&aux_moment);
    *secs_since_midnite_p = aux_moment.time.seconds + 60 * (aux_moment.time.minutes) + 3600 * (aux_moment.time.hours);
    aux_days = 0;
    for (i=0; i < aux_moment.date.month ; i++)
        aux_days+= month_days[i];
    aux_days+= aux_moment.date.day;
    *days_since_ny_p = aux_days;
    *years_since_2000_p = aux_moment.date.year - 2000;
}


void control_do_use_photocell(int phc_on)
{
    if(phc_on)
    {
        flag_use_photocell=1;
    }
    else
    {
        flag_use_photocell=0;
        if(current_mode==ASTRO_CLK || current_mode==SCHEDULE_PLAN)
        output_set_light_level(event_get_dim_level(current_mode,event_calculate_current_index(current_mode)));
    }
}

void control_do_on()
{

    output_set_light_level(100);

}

void control_do_off()
{

   output_set_light_level(0);

}

void control_do_dim (int nivel)
{
    if (nivel>100)
        output_set_light_level(100);
    else if (nivel<0)
        output_set_light_level(0);
    else 
        output_set_light_level(nivel);

}


void control_do_set_mode(type_mode mode_in)
{
        current_mode=mode_in;
        if (mode_in == ASTRO_CLK || mode_in == SCHEDULE_PLAN)
        {
        event_save_index(mode_in);
        output_set_light_level(event_get_dim_level(mode_in,event_calculate_current_index(mode_in)));
        }
}



void control_do_get_mode(type_mode* mode_p, int* phc_flag_p, int* dim_lvl_p)
{
    *mode_p = current_mode;
    *phc_flag_p = flag_use_photocell;
    *dim_lvl_p = output_get_light_level();
}

void control_do_add_event(int timer_is_weekend, int mins_since_midnite, int dim_lvl)
{
    int h,m;
    h = mins_since_midnite / 60;
    m = mins_since_midnite % 60;
    event_add(timer_is_weekend, event_create(h,m,dim_lvl));
}

void control_do_del_event(int timer_is_weekend, int ID)
{
    if (ID>=0 && ID<MAX_SP_EVENTS)
    {
        event_remove (timer_is_weekend, ID);
    }

}

void control_do_res_event(int timer_is_weekend)
{
    event_reset(timer_is_weekend);
}

void control_do_dir_event(int ID_list[32], int mins_since_midnite_list[32], int dim_lvl_list[32], int days)
{

    type_time aux_time;
    int i, index, mins_since_midnite;
    type_event event;

    //Lleno las listas con nulo
    for (i=0 ; i<32 ; i++)
    {
        ID_list[i] = -1;
        mins_since_midnite_list[i] = -1;
        dim_lvl_list[i] = -1;
    }
    switch (days)
    {
        case SE:
        {
            //Dawn
            sun_equation_get_sunrise(&aux_time);
            mins_since_midnite = 60*(aux_time.hours) + aux_time.minutes;
            ID_list[0] = 0;
            mins_since_midnite_list[0] = mins_since_midnite;
            dim_lvl_list[0] = 0;
            //Dusk
            sun_equation_get_sunset(&aux_time);
            mins_since_midnite = 60*(aux_time.hours) + aux_time.minutes;
            ID_list[1] = 1;
            mins_since_midnite_list[1] = mins_since_midnite;
            dim_lvl_list[1] = 100;
            break;
        }
        case WD:
        {
            i = 0;
            for (index = 0 ; index < 32 ; index++)
            {
                event_get(&event, WD ,index);
                if (event.event_time.hours >= 0)
                {
                    mins_since_midnite = 60*(event.event_time.hours) + event.event_time.minutes;
                    ID_list[i] = index;
                    mins_since_midnite_list[i] = mins_since_midnite;
                    dim_lvl_list[i] = event.dim_lvl;
                    i++;
                }
            }
            break;
        }
        case WE:
        {
            i = 0;
            for (index = 0 ; index < 32 ; index++)
            {
                event_get(&event, WE ,index);
                if (event.event_time.hours >= 0)
                {
                    mins_since_midnite = 60*(event.event_time.hours) + event.event_time.minutes;
                    ID_list[i] = index;
                    mins_since_midnite_list[i] = mins_since_midnite;
                    dim_lvl_list[i] = event.dim_lvl;
                    i++;
                }
            }
            break;
        }
    }
}

    void control_do_get_volt(float* volt){
        *volt=acs71020_get_voltage();
    }

    void control_do_get_curr(float* curr){
        *curr=acs71020_get_current();
    }

    void control_do_get_actpwr(float* actpwr){
        *actpwr=acs71020_get_actpwr();
    }

    void control_do_get_apppwr(float* apppwr){
        *apppwr=acs71020_get_apppwr();
    }

    void control_do_get_reapwr(float* reapwr){
        *reapwr=acs71020_get_reapwr();
    }

    void control_do_get_pfactor(float* pfactor){
        *pfactor=acs71020_get_pfactor();
    }
