/*
 * funciones_auxiliares.c
 *
 *  Created on: Mar 21, 2019
 *      Author: jschandy
 */

/**
 * Ansi C "itoa" based on Kernighan & Ritchie's "Ansi C":
 */

#include <utils.h>


#define BASE 10

char ascii2byte(char mostSignificant, char lessSignificant){
    char aux1,aux2;
    if (mostSignificant>='0' && mostSignificant<='9'){ //es numero
        aux1=mostSignificant-'0';
    }
    if(mostSignificant>='A' && mostSignificant<='F'){//es letra
        aux1=mostSignificant - 'A' +10;
    }

    if (lessSignificant>='0' && lessSignificant<='9'){ //es numero
        aux2=lessSignificant-'0';
    }
    if(lessSignificant>='A' && lessSignificant<='F'){//es letra
        aux2=lessSignificant - 'A' +10;
    }
    return((aux1<<4)+aux2);

}

void byte2ascii(char byte_in, char * char_out){
    char auxLS,auxMS;

    auxLS=byte_in & 0x0F;
    auxMS=byte_in>>4;

    if (auxLS>=0 && auxLS<=9){ //es numero
        auxLS=auxLS+'0';
    }
    if(auxLS>=0x0A && auxLS<=0x0F){//es letra
        auxLS=auxLS + 'A' -10;
    }

    if (auxMS>=0 && auxMS<=0x09){ //es numero
        auxMS=auxMS +'0';
    }
    if(auxMS>=0x0A && auxMS<=0x0F){//es letra
        auxMS=auxMS + 'A' -10;
    }

    char_out[0]=auxMS;
    char_out[1]=auxLS;

}

void strreverse(char* begin, char* end) {

    char aux;
    while(end>begin)
        aux=*end, *end--=*begin, *begin++=aux;
}

int strcmp(const char* s1, const char* s2)
{
    while(s1 && (s1==*s2))
        s1++,s2++;
    return (const unsigned char)s1-(const unsigned char)s2;
}

void itoa(int value, char* str) {
    static char num[] = "0123456789abcdefghijklmnopqrstuvwxyz";
    char* wstr=str;
    int sign;
    // Take care of sign
    if ((sign=value) < 0) value = -value;
    // Conversion. Number is reversed.
    do *wstr++ = num[value%BASE]; while(value/=BASE);
    if(sign<0) *wstr++='-';
    //*wstr='\0';
    // Reverse string
    strreverse(str,wstr-1);
}


char *strcpy(char * dest, const char * src)
{
     char       *d = dest;
     const char *s = src;

     while (*d++ = *s++);
     return dest;
}
