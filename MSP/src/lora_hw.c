/*
 * loraHW.c
 *
 *  Created on: Apr 15, 2020
 *      Author: tomas
 */

#include <lora_hw.h>
uint8_t loraHWTxBuff[MAX_SIZE_loraHW_MSG];
uint8_t loraHWRxBuff[MAX_SIZE_loraHW_MSG];
volatile static unsigned int loraHWRxHead, loraHWTxHead;
volatile static unsigned int lora_rx_complete;
volatile static unsigned int lora_tx_complete = 1;

void loraHWHandleRx(char* extBuff);
void loraHWBeginTx(char* extBuff);
bool status;

const eUSCI_UART_Config uartConfig = {
EUSCI_A_UART_CLOCKSOURCE_SMCLK,                // SMCLK Clock Source con DCO
        13,                                              // BRDIV = 13
        0,                                              // UCxBRF = 0
        0x25,                                           // UCxBRS = 0x25
        EUSCI_A_UART_NO_PARITY,                         // No Parity
        EUSCI_A_UART_LSB_FIRST,                         // LSB First
        EUSCI_A_UART_ONE_STOP_BIT,                      // One stop bit
        EUSCI_A_UART_MODE,                              // UART mode
        EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION  // Oversampling
        };

void loraHW_init(void)
{
    /* configuración de comunicación loraHW pines: 3.2 RX, 3.3 TX, BAUDRATE 57600*/

    /* Selecting P3.2 and P3.3 in UART mode */
    MAP_GPIO_setAsPeripheralModuleFunctionInputPin(
            GPIO_PORT_P3,
            GPIO_PIN2 | GPIO_PIN3,
            GPIO_PRIMARY_MODULE_FUNCTION);

    /*el DCO es el source de SMCLK default*/
    CS_setDCOCenteredFrequency(CS_DCO_FREQUENCY_12);

    /* Configuring UART Module */
    status = MAP_UART_initModule(EUSCI_A2_BASE, &uartConfig);

    /* Enable UART module */
    MAP_UART_enableModule(EUSCI_A2_BASE);

    UART_enableInterrupt(EUSCI_A2_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT);
}

void loraHWSendCommand(char* extBuff)
{
    lora_tx_complete = 0;
    loraHWBeginTx(extBuff);
}

void loraHWCommandResponse(char* extbuff)
{
    lora_rx_complete = 0;
    loraHWHandleRx(extbuff);
}

uint8_t loraHWRxAvailable()
{
    return lora_rx_complete;
}
uint8_t loraHWTxAvailable()
{
    return lora_tx_complete;
}

/*-----------------------------------------------------------------------------------------------------------------*/

void loraHWHandleRx(char* extBuff)
{
    do
    {
        extBuff[loraHWRxHead] = loraHWRxBuff[loraHWRxHead];
        loraHWRxHead++;
    }
    while (loraHWRxBuff[loraHWRxHead - 1] != '\r');
    loraHWRxHead = 0;
}

void loraHWBeginTx(char* extBuff)
{
    int n = 0;

    do
    {
        loraHWTxBuff[n] = extBuff[n];
        n++;
    }
    while (n < MAX_SIZE_loraHW_MSG && extBuff[n - 1] != '\r');

    loraHWTxBuff[n] = '\n';

    lora_tx_complete = 0;

    UART_enableInterrupt(EUSCI_A2_BASE, EUSCI_A_UART_TRANSMIT_INTERRUPT);
    EUSCI_A_CMSIS(EUSCI_A2_BASE)->TXBUF = loraHWTxBuff[0];


}

int x = 0;
void EUSCIA2_IRQHandler()
{
    int status = MAP_UART_getEnabledInterruptStatus(EUSCI_A2_BASE);

    if (status & EUSCI_A_UART_RECEIVE_INTERRUPT_FLAG)
    {
        if ((UCA2RXBUF==0x0A) && loraHWRxHead==0){return;}

        loraHWRxBuff[loraHWRxHead] = UCA2RXBUF;

        if (loraHWRxBuff[loraHWRxHead] == '\r' || (loraHWRxHead >= MAX_SIZE_loraHW_MSG))
        {
            loraHWRxHead = 0;
            lora_rx_complete = 1;

        }
        else
        {
            loraHWRxHead++;
            lora_rx_complete = 0;
        }
    }

    if (status & EUSCI_A_UART_TRANSMIT_INTERRUPT_FLAG)
    {
        if ((loraHWTxHead < MAX_SIZE_loraHW_MSG) && !(loraHWTxBuff[loraHWTxHead - 1] == '\n'))
        {
            EUSCI_A_CMSIS(EUSCI_A2_BASE)->TXBUF = loraHWTxBuff[loraHWTxHead];
            loraHWTxHead++;
            lora_tx_complete = 0;
        }
        else
        {
            loraHWTxHead = 1;
            UCA2IE &= ~UCTXIE;
            EUSCI_A2->IFG &= ~EUSCI_A_IFG_TXIFG;
            lora_tx_complete = 1;
        }
    }
    if (status & EUSCI_A_UART_RECEIVE_ERRONEOUSCHAR_INTERRUPT)
    {
           x++;
    }

}

