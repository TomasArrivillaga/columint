/*
 * LORA.c
 *
 *  Created on: Apr 20, 2020
 *      Author: tomas
 */
#include <LORA.h>

volatile char SYS_RESET[MAX_SIZE_loraHW_MSG]                =   "sys reset\r\n";

//otaa
volatile char MAC_SET_APPEUI[MAX_SIZE_loraHW_MSG]           =   "mac set appeui 70B3D57ED002DAE9\r\n";
volatile char MAC_SET_DEVEUI[MAX_SIZE_loraHW_MSG]           =   "mac set deveui 000000000000000E\r\n";
volatile char MAC_SET_APPKEY[MAX_SIZE_loraHW_MSG]           =   "mac set appkey 2D158887995CC3431B4BF47C826CAB23\r\n";

volatile char MAC_JOIN_OTAA [MAX_SIZE_loraHW_MSG]           =   "mac join otaa\r\n";
volatile char MAC_JOIN_ABP  [MAX_SIZE_loraHW_MSG]           =   "mac join abp\r\n";
volatile char MAC_GET_STATUS[MAX_SIZE_loraHW_MSG]           =   "mac get status\r\n";
volatile char RADIO_SET_SF_SF8[MAX_SIZE_loraHW_MSG]         =   "mac set rx2 8 923300000\r\n";
volatile char MAC_SET_DR_4[MAX_SIZE_loraHW_MSG]             =   "mac set dr 4\r\n";
volatile char MAC_SET_CH_STATUS_X_ON[MAX_SIZE_loraHW_MSG]   =   "mac set ch status a on\r\n"; //a en pos 18
volatile char MAC_SET_CH_STATUS_X_OFF[MAX_SIZE_loraHW_MSG]  =   "mac set ch status a off\r\n"; //a en pos 18
volatile char MAC_SET_CH_STATUS_XX_OFF[MAX_SIZE_loraHW_MSG] =   "mac set ch status aa off\r\n";



uint8_t loraDownlink(char* extBuff){
    uint8_t i=7;
    uint8_t j=0;
    uint8_t isLoraDownlink=0;
    char midBuff[MAX_SIZE_loraHW_MSG];

    if(     extBuff[0] == 'm' &&
            extBuff[1] == 'a' && extBuff[2] == 'c' && extBuff[3] == '_' &&
            extBuff[4] == 'r' && extBuff[5] == 'x' && extBuff[6] == ' ' )
    {
        isLoraDownlink=1;
        while(extBuff[i]!=' '){
            i++;
        }
        i++;

        for (j=0;j<MAX_SIZE_loraHW_MSG;j++){
            midBuff[j]=extBuff[j];
        }
        j=0;

        do {
            extBuff[j]=midBuff[i];
            j++;
            i++;

        }while (extBuff[i-1]!='\r');

        i=0;

        do{
            extBuff[i]=ascii2byte(extBuff[2*i],extBuff[(2*i)+1]);
            i++;
        }while(extBuff[2*i]!='\r');

    }
    return (isLoraDownlink);
}

void sendCommandAndWaitForOk(char* stringToSend){  //sirve para todos los mensajes cuyo string es estatico y la respuesta debe ser ok para continuar
    uint8_t ok,Retry;
    Retry=1;
    ok=0;
    volatile char response[MAX_SIZE_loraHW_MSG];

    while(!ok){
        if( loraHWTxAvailable() && Retry){
            loraHWSendCommand(stringToSend);
            Retry=0;
        }
        __delay_cycles(100);
        if( loraHWRxAvailable()){

            __disable_interrupt();
            loraHWCommandResponse(response);
            __enable_interrupt();

            ok=(response[0]=='o' && response[1]=='k')? 1:0;
            Retry=1;
        }
    }


}

void loraSendBytes (char* bytes, uint8_t byteqty)
{
    volatile char aux[MAX_SIZE_loraHW_MSG] =   "mac tx uncnf ";
    volatile char auxC[MAX_SIZE_loraHW_MSG];
    uint8_t i=0;
    uint8_t auxint;
    uint8_t offset=0;
    uint8_t EventQty;
    uint8_t MsgQty;
    uint8_t j=0;
    uint8_t top;

    auxint=bytes[byteqty];

    if(auxint == 1 || auxint==6 || auxint == 11){
          auxint=bytes[byteqty];
          EventQty=byteqty/3;
          MsgQty=EventQty/2 + EventQty%2;
          MsgQty=(MsgQty>5)? 5:MsgQty;

          for(j=0;j<MsgQty;j++){
              auxint=auxint+j;
              if (auxint<10)
              {
                  aux[13]=auxint + '0';
                  aux[14]=' ';
                  aux[15]=0;
                  offset=0;
              }else if(auxint>=10 && auxint<20)
              {
                  aux[13]='1';
                  aux[14]=auxint -10 +'0';
                  aux[15]=' ';
                  aux[16]=0;
                  offset=1;
              }else if(auxint>=20 && auxint<30)
              {
                  aux[13]='2';
                  aux[14]=auxint -20 +'0';
                  aux[15]=' ';
                  aux[16]=0;
                  offset=1;
              }

          if(EventQty>1){
              top=2;
              EventQty=EventQty-2;
          }else{
              top=1;
          }
              for(i=0;i<(top*3);i++){
                  byte2ascii(bytes[i+(j*6)], auxC+(2*i));
              }

              auxC[2*i+2]=0;
              strcat(aux,auxC);
              aux[15+ offset + (3*2*top)]=0;
              strcat(aux,"\r\n");
              sendCommandAndWaitForOk(aux);
              loraChannelSelect();
          }
    }else{

        if (auxint<10)
        {
            aux[13]=auxint + '0';
            aux[14]=' ';
        }else if(auxint>=10 && auxint<20)
        {
            aux[13]='1';
            aux[14]=auxint -10 +'0';
            aux[15]=' ';
            offset=1;
        }else if(auxint>=20 && auxint<30)
        {
            aux[13]='2';
            aux[14]=auxint -20 +'0';
            aux[15]=' ';
            aux[16]=0;
            offset=1;
        }

        for(i=0;i<byteqty;i++){
            byte2ascii(bytes[i], auxC+(2*i));
        }

        auxC[2*i+2]=0;
        strcat(aux,auxC);
        aux[15+ offset + 2*byteqty]=0;
        strcat(aux,"\r\n");


        sendCommandAndWaitForOk(aux);
    }
}

void sendResetAndWaitForOk(void){
    uint8_t ok=0;
    uint8_t Retry=1;
    volatile char response[MAX_SIZE_loraHW_MSG];
    while(!ok){
        if( loraHWTxAvailable() && Retry){
            loraHWSendCommand(SYS_RESET);
            Retry=0;
        }
        if( loraHWRxAvailable()){

            __disable_interrupt();
            loraHWCommandResponse(response);
            __enable_interrupt();

            ok=(response[0]=='R' && response[1]=='N')? 1:0;
            Retry=1;
        }
    }
}



void loraChannelSelect(void){
    volatile int i,j;

    for(i=0;i<10;i++){
        MAC_SET_CH_STATUS_X_OFF[18]=48+i;
        MAC_SET_CH_STATUS_X_ON[18]=48+i;
        if(i<8) {
            sendCommandAndWaitForOk(MAC_SET_CH_STATUS_X_ON);
        }else{
            sendCommandAndWaitForOk(MAC_SET_CH_STATUS_X_OFF);
        }
    }
    i=0;
    j=1;

    for(j=1;j<7;j++){
        MAC_SET_CH_STATUS_XX_OFF[18]=48+j;
        for(i=0;i<10;i++){
            if((j*10)+i<65){
                MAC_SET_CH_STATUS_XX_OFF[19]=48+i;
                sendCommandAndWaitForOk(MAC_SET_CH_STATUS_XX_OFF);
            }
        }
    }
}

void sendJoinOTAAAndWaitForOk(){

    uint8_t ok=0;
    uint8_t Retry=1;
    uint8_t step=0;
    volatile char response[MAX_SIZE_loraHW_MSG];

    while(!ok)
    {
        if( loraHWTxAvailable() && Retry){
            loraHWSendCommand(MAC_JOIN_OTAA);
            Retry=0;
            step=1;
        }
        if( loraHWRxAvailable() && step==1){
            __disable_interrupt();
            loraHWCommandResponse(response);
            __enable_interrupt();
            step=2;
        }
        if( loraHWRxAvailable() && step==2){
            __disable_interrupt();
            loraHWCommandResponse(response);
            __enable_interrupt();
            ok=(response[0]=='a' && response[1]=='c')? 1:0;
            Retry=1;
        }

    }
}


void loraInit(void){

    sendResetAndWaitForOk();
    sendCommandAndWaitForOk(MAC_SET_APPEUI);
    sendCommandAndWaitForOk(MAC_SET_DEVEUI);
    sendCommandAndWaitForOk(MAC_SET_APPKEY);
    sendJoinOTAAAndWaitForOk();
    loraChannelSelect();
}

