/*
 * acs71020.c
 *
 *  Created on: 28 jul. 2020
 *      Author: Emilio
 */

#include <acs71020_hw.h>
#include <acs71020.h>
extern uint8_t acs_TxData[NUM_OF_TX_BYTES];
extern uint8_t acs_RxData[NUM_OF_REC_BYTES];

float acs71020_get_voltage(){
	float aux_v=0;

    acs71020_read(REG_IRMS_VRMS);
    aux_v=((float)(acs_RxData[1]*256+acs_RxData[0]))/0x8000;
    aux_v=aux_v*(FULL_SCALE_V);


    return aux_v;
}

float acs71020_get_current(){
	float aux_i=0;

    acs71020_read(REG_IRMS_VRMS_PROM);
    aux_i=((float)(acs_RxData[3]*256+acs_RxData[2]));
    aux_i=aux_i*(FULL_SCALE_I)/0x4000;

    return aux_i;
}


float acs71020_get_actpwr(){
	float aux_pwr=0;

    acs71020_read(REG_ACTPWR_PROM);
    if (acs_RxData[2]&0x01){ //Es negativo
    	aux_pwr=(float)(acs_RxData[1]*256+acs_RxData[0])-0x20000;
    }

    else{ //Es positivo
    	aux_pwr=((float)(acs_RxData[1]*256+acs_RxData[0]));
    }

    aux_pwr=aux_pwr*(FULL_SCALE_V*FULL_SCALE_I)/0x8000;

    return aux_pwr;
}

float acs71020_get_apppwr(){
	float aux_pwr=0;

    acs71020_read(REG_APPPWR);
    aux_pwr=((float)(acs_RxData[1]*256+acs_RxData[0]));
    aux_pwr=aux_pwr*(FULL_SCALE_V*FULL_SCALE_I)/0x8000;

    return aux_pwr;
}

float acs71020_get_reapwr(){
	float aux_pwr=0;

    acs71020_read(REG_REAPWR);
    aux_pwr=((float)(acs_RxData[1]*256+acs_RxData[0]));
    aux_pwr=aux_pwr*(FULL_SCALE_V*FULL_SCALE_I)/0x8000;

    return aux_pwr;
}

float acs71020_get_pfactor(){
	float aux_pfactor=0;

    acs71020_read(REG_PFACTOR);
    if (acs_RxData[1]&0x04){ //Es negativo
    	aux_pfactor=(float)(acs_RxData[1]*256+acs_RxData[0])-0x0800;
    }

    else{ //Es positivo
    	aux_pfactor=((float)(acs_RxData[1]*256+acs_RxData[0]));
    }

    aux_pfactor=aux_pfactor*(FULL_SCALE_V*FULL_SCALE_I)/0x800;

    return aux_pfactor;
}
