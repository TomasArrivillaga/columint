#include <sun_equation.h>

static type_time Sunset,Sunrise;

void deg2HMSm(float deg, type_time* aux_retorno);

int days_since_2000(){
	type_moment m_now;
	int n,aux,i;
	timer_get_moment(&m_now);

	aux=m_now.date.year - 2000;
	n=365;
	for (i=1;i<aux;i++)
		{
			if (i % 4==0)
			{
				n+=366;
			}else{
				n+=365;
			}
		}

	n+=timer_get_days_since_new_year();
	return(n);
}

float calc_julian_date()
{
	return(days_since_2000() + 2451545 - 0.0008);
}

float mean_solar_noon(float lon)
{
	return(days_since_2000() - lon/360);
}

float solar_mean_anomaly (float lon){
    int M;
    float aux;

	aux=(357.5291 + (0.98560028*mean_solar_noon(lon)));

    while(aux > 360)
    {
        aux-=360;
    }
	M=aux;
	return(M);
}

float sind(double x){
    double aux;

    aux=sin(x*M_PI/180);
    return (aux);     //que la entrada al seno sea en grados
}

float cosd (double x){
    double aux;

    aux=cos(x*M_PI/180);
    return (aux);     //que la entrada al seno sea en grados
}

float equation_of_the_center(int M)
{
	float C;
	C= 1.9148*sind(M) + 0.0200*sind(2*M) + 0.0003*sind(3*M);
	return(C);
}

float ecliptic_longitud(float lon)
{
	float lambda,C,aux;
	int M;
	M=solar_mean_anomaly(lon);
	C=equation_of_the_center(lon);

	aux= M+C+180+102.9372;


	while(aux > 360)
	{
	    aux-=360;
	}
	lambda= aux;
	return(lambda);
}

float solar_transit(float lon)
{
	int M,lambda;
	float J,JT;

	M=solar_mean_anomaly(lon);
	lambda=ecliptic_longitud(lon);
	J=mean_solar_noon(lon);

	JT=/*2451545*/ J + 0.0053*sind(M) -0.0069*sind(2*lambda);

	return(JT);
}

float declination_of_the_sun(float lon)
{
	float lambda,delta;

	lambda=ecliptic_longitud(lon);

	delta=(180/M_PI)*asin(sind(lambda)*sind(23.44));
	return(delta);
}

float Hour_angle(float delta, float lat)
{
	float w;

	w=sind(-0.83) - (sind(lat)*sind(delta));
	w=w/(cosd(lat)*cosd(lat));
	w=(180/M_PI)*acos(w);

	return (w);
}


void sun_equation_calc_sun_events(float lat,float lon, int time_zone)
{
	float delta,Jtransit,w,aux2;
	type_time aux ={0,0,0};
	int hora,minutos;

	Jtransit=solar_transit(lon);
	delta=declination_of_the_sun(lon);

	w=Hour_angle(delta,lat);

	deg2HMSm(w,&aux);

	aux2=Jtransit - floor(Jtransit);

	hora=0;
	minutos=0;

	while (aux2>0.04167)
	{
		aux2-=0.04167;
		hora++;
	}

	while (aux2>0.0006945)
	{
		aux2-=0.0006945;
		minutos++;
	}

	hora+=12;


	Sunrise.hours		= hora + time_zone - aux.hours;
	    if (minutos>aux.minutes)
	    {
	        Sunrise.minutes		= minutos - aux.minutes;
	    }else{
	        Sunrise.minutes     = minutos + 60 -aux.minutes;
	        Sunrise.hours-=1;
	    }


	Sunrise.seconds		= 0 ;//60 - aux.seconds;

	Sunset.hours		= hora + time_zone + aux.hours;
	if (minutos+aux.minutes>60){
	    Sunset.minutes      = minutos + aux.minutes -60;
	    Sunset.hours+=1;
	}else{
	    Sunset.minutes      = minutos + aux.minutes;
	}
	Sunset.seconds		= 0 ;//60 + aux.seconds;

}





void deg2HMSm(float deg, type_time* aux_retorno)
{ 
	int aux1,aux2;

	aux2=deg*4;                  //deg/0.25 pasa a minutos la hora angular.

	(*aux_retorno).hours=aux2/60;       // minutos /60 a horas
	aux1=(aux2)%60;                     // el resto queda en minutos

	(*aux_retorno).minutes=aux1/1;      //minutos /1 nos da minutos
	aux1=aux1%1;       // el resto se usa para redondear.

}

void sun_equation_get_sunset(type_time* sunset_aux)
{
	*(sunset_aux)=Sunset;
	timer_time_dec_minutes(sunset_aux,12); //por error en calculo de sun_equation
}

void sun_equation_get_sunrise(type_time* sunrise_aux)
{
	*(sunrise_aux)=Sunrise;
	timer_time_inc_minutes(sunrise_aux,12); //por error en calculo de sun_equation
}

