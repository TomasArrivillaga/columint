/**
 * @file acs71020_hw.h
 * @brief Este modulo agrupa las funciones que configuran y manejan la comunicacion I2C con el chip acs71020.
 *
 * 
 * Modulo acs71020_hw
 *
 *    
 *	 
 *  
 * 
 *
* acs71020_hw.c

* @version 2.0
* @author  	Emilio Albarrracin 	<ealbarracion2911@gmail.com>
* @author	Tomas Arrivillaga	<tomas92@ymail.com>
* @author	Felipe Moran		<felipemoran73@gmail.com>
* @date 4 de Agosto 2020
*
* @date 4 de Agosto 2020
**/
//*****************************************************************************
//
//!
//! \addtogroup acs71020_module
//! @{
//
//*****************************************************************************

#ifndef INCLUDE_ACS71020_HW_H_
#define INCLUDE_ACS71020_HW_H_

#include <driverlib.h>

/** @brief direccion I2C del acs71020*/
#define SLAVE_ADDRESS       0x45
/** @brief numero de bytes para buffer de recepcion*/
#define NUM_OF_REC_BYTES    4
/** @brief numero de bytes para buffer de transmision*/
#define NUM_OF_TX_BYTES    5

/** @brief numero de registro del acs71020 de voltaje y corriente*/
#define REG_IRMS_VRMS			0x20
/** @brief numero de registro del acs71020 de potencia activa*/
#define REG_ACTPWR				0x21
/** @brief numero de registro del acs71020 de potencia aparente*/
#define REG_APPPWR				0x22
/** @brief numero de registro del acs71020 de potencia reactiva*/
#define REG_REAPWR				0x23
/** @brief numero de registro del acs71020 de factor de potencia*/
#define REG_PFACTOR				0x24
/** @brief numero de registro del acs71020 de voltaje y corriente promediadas*/
#define REG_IRMS_VRMS_PROM		0x26
/** @brief numero de registro del acs71020 de potencia activa promediada*/
#define REG_ACTPWR_PROM			0x28

#define FULL_SCALE_V	535.0
#define FULL_SCALE_I	30.0

/**
  @brief configura la comunicacion I2C con el acs71020
  @param void
  @return void
*/
void acs71020_config();

/**
  @brief escribe al acs71020
  @param register_id numero de registro a escribir
  @param data puntero a datos a escribir
  @return void
*/
void acs71020_write (uint8_t register_id, uint8_t* data);

/**
  @brief lee del acs71020
  @param register_id numero de registro a leer
  @return void
*/
void acs71020_read(uint8_t register_id);
//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************

#endif /* INCLUDE_ACS71020_HW_H_ */
