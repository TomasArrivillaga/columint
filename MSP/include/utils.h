/**
 * @file utils.h
 * @brief Este modulo agrupa las funciones auxiliares para el manejo de strings y caracteres.
 *
 * 
 * 
 *
 *    
 *	 
 *  
 * 
 *
* utils.c

* @version 2.0
* @author  	Emilio Albarrracin 	<ealbarracion2911@gmail.com>
* @author	Tomas Arrivillaga	<tomas92@ymail.com>
* @author	Felipe Moran		<felipemoran73@gmail.com>
* @date 4 de Agosto 2020
*
* @date 4 de Agosto 2020
**/


#ifndef FUNCIONES_AUXILIARES_H_INCLUDED
#define FUNCIONES_AUXILIARES_H_INCLUDED

#include <timer.h>

/**
 @brief convierte un entero a ASCII
 @param value valor entero a covertir
 @param str puntero a char donde guardar el resultado
 @return void.
*/

void itoa(int value, char* str);

/**
 @brief Concatena dos cadenas de caracteres.
 @param string1 primera cadena de caracteres.
 @param string2 segunda cadena de caracteres.
 @return 
*/

char *strcat(char * restrict string1,const char * restrict string2);

/**
 @brief Copia una cadena de caracteres de un arreglo a otro.
 @param dest arreglo destino.
 @param src arreglo fuente.
 @return retorna el arreglo destino.
*/

char *strcpy(char * dest, const char * src);

/**
 @brief Compara dos cadenas de caracteres.
 @param s1 primera cadena de caracteres.
 @param s2 segunda cadena de caracteres.
 @return retorna 0 si son iguales.
*/

int strcmp(const char* s1, const char* s2);

/**
 @brief Convierte un byte expresado en ascii (por ejemplo "A3") en el byte (char) A3.
 @param mostSignificant caracter mas significativo.
 @param lessSignificant caracter menos significativo.
 @return byte convertido.
*/

char ascii2byte(char mostSignificant, char lessSignificant);

/**
 @brief Toma un byte y lo expresa en ascii.
 @param byte_in byte a convertir.
 @param char_out arreglo en el que se escribira el byte expresado en ascii.
 @return byte convertido.
*/

void byte2ascii(char byte_in, char * char_out);
#endif // FUNCIONES_AUXILIARES_H_INCLUDED
