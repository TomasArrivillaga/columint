/**
 * @file photocell_hw.h
 * @brief Este modulo agrupa las funciones que configuran y manejan el ADC14 utilizado para obtener medidas de la fotocelula.
 *
 * 
 * Modulo photocell_hw
 *
 *     
 *
 *  
 * 
 *
 * photocell_hw.c

* @version 2.0
* @author   Emilio Albarrracin  <ealbarracin2911@gmail.com>
* @author   Tom�s Arrivillaga   <tomas92@ymail.com>
* @author   Felipe Mor�n        <felipemoran73@gmail.com>
* @date 	4 de Agosto 2020
*
*/
//*****************************************************************************
//
//!
//! \addtogroup photocell_module
//! @{
//
//*****************************************************************************

#ifndef INCLUDE_PHOTOCELL_HW_H_
#define INCLUDE_PHOTOCELL_HW_H_
#include <msp.h>
#include <driverlib.h>



/**
	@brief Inicializa el ADC14 y habilita sus interrupciones.
  	@param void
  	@return void 
*/

void phc_init ();

/**
	@brief obtiene el valor de la ultima medida obtenida por el ADC14.
  	@param void.
  	@return Medida del ADC14 en cantidad de pasos. 
*/

uint_fast16_t phc_GetLastValue(void);

/**
	@brief hace que el ADC14 dispare una medida
  	@param void
  	@return void 
*/

void phcTriggerMeasure(void);
//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************

#endif /** INCLUDE_PHOTOCELL_H_ */
