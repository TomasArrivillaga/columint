/**
 * @file         control.h                                                                                                                
 * @brief        Modulo que agrupa tipo y funcion para toma de decisiones de funcionamiento en base al modo actual y banderas
 * 
 * @author  Tomas Arrivillaga         <tomas92@ymail.com>
 * @author  Emilio Albarraci­n         <ealbarracin2911@gmail.com>
 * @author  Felipe Moran                <Felipemoran73@gmail.com>
 *
 * @version 2.0
 *
 * @date 4 Agosto 2020
 *
 */
//*****************************************************************************
//
//!
//! \addtogroup control_module
//! @{
//
//*****************************************************************************


#ifndef INCLUDE_CONTROL_H
#define INCLUDE_CONTROL_H


#include <output.h>
#include <photocell.h>
#include <utils.h>
#include <UART.h>
#include <timer.h>
#include <acs71020.h>

/** @brief definicion de tipo de los modos de funcionamiento*/
typedef enum
{
    DIRECT,
    SCHEDULE_PLAN,
    ASTRO_CLK,
    PHOTOCELL
} type_mode;

#include <event_management.h>

/**
        @brief Funcion que decide que acción tomar basado en el modo actual y banderas.
        @param void
        @return void
*/
void control_take_action();
/**
        Esta funcion recibe el momento, verifica que sea valido y si lo es, lo fija como fecha y hora del sistema. Si los datos no son correctos la funcion no toma ninguna accion.
        @brief modifica el momento actual en funcion de lo recibido como parametros.
        @param years_since_2000 años que pasaron desde el 2000. Por ejemplo, si se desea que el año del sistema sea 2022, years_since_2000 = 22.
        @param days_since_ny dias que pasaron desde que inicio el año. Por ejemplo, si se desea que el dia del sistema sea 3 de febrero, days_since_ny = 34.
        @param secs_since_midnite segundos que pasaron desde la media noche. Por ejemplo, si se desea que la hora sistema sea 00:05:31, secs_since_midnite = 331.
        @return void
*/
void control_do_set_moment(int years_since_2000, int days_since_ny, int secs_since_midnite);

/**
        
        @brief Prende la luminaria.
        @param void
        @return void
*/
void control_do_on();

/**
        
        @brief Apaga la luminaria.
        @param void
        @return void
*/
void control_do_off();

/**
        Pone la luminaria en el nivel indicado por nivel. Si el nivel es mayor a 100 se acota en 100, si es menor a 0 se pone en 0. 
        @brief Prende la luminaria al nivel de luz deseado.
        @param nivel Nivel de luz deseado.
        @return void
*/
void control_do_dim (int nivel);

/**
        Cambia el modo al modo deseado. Cuando se cambia a un modo que se rige por eventos (ASTRO_CLK o SCHEDULE_PLAN), pasa con el nivel de luz del 
        evento anterior temporalmente. Si en la lista de eventos, no hay eventos, se apaga la luz.

        @brief Cambia la luminaria al modo deseado
        @param mode_in Modo deseado.
        @return void
*/
void control_do_set_mode(type_mode mode_in);

/**

        @brief Escribe en los punteros que se pasan como parametro el modo actual, si se esta tomando en cuenta a la fotocelula o no y el nivel actual de la luz.
        @param mode_p puntero a la direccion donde la funcion escribira el modo actual.
        @param phc_flag_p puntero a la direccion donde la funcion escribira si se esta utilizando la fotocelula o no.
        @param dim_lvl_p puntero a la direccion donde la funcion escribira el nivel de luz actual.
        @return void
*/
void control_do_get_mode(type_mode* mode_p, int* phc_flag_p, int* dim_lvl_p);


/**
        @brief Agrega un evento al arreglo correspondiente (WD o WE). Si el arreglo esta lleno, o el evento ya esta en el arreglo, la funcion no hace nada.
        @param is_weekend bandera que indica si es fin de semana.
        @param mins_since_midnite hora del evento en minutos. Por ejemplo, si la hora es 05:15:00, mins_since_midnite = 315.
        @param dim_lvl nivel de luz del evento.
        @return void
*/
void control_do_add_event(int is_weekend, int mins_since_midnite, int dim_lvl);

/**
        @brief Elimina un evento del arreglo correspondiente (WD o WE). Si el indice a eliminar no pertenece al arreglo o no hay un evento con el indice,
        la funcion no hace nada.
        @param is_weekend bandera que indica si se agrega el evento a WD o a WE.
        @param ID numero identificador del evento a borrar.
        @return void
*/
void control_do_del_event(int is_weekend, int ID);

/**
        @brief Elimina todos los eventos de un arreglo.
        @param is_weekend bandera que indica si se borran los eventos de WD o de WE.
        @return void
*/
void control_do_res_event(int is_weekend);

/**
        @brief Escribe en los arreglos que se le pasan como argumento la lista de todos los eventos en el registro que se pide.
        @param ID_list lista en la cual la funcion escribe los ID de los eventos.
        @param mins_since_midnite_list lista de horas de los eventos, expresados como minutos desde la medianoche. Por ejemplo, si la hora del evento i es 05:15:00, mins_since_midnite[i] = 315.
        @param dim_lvl_list lista de niveles de dimerizacion de los eventos.
        @param days indica a que arreglo se refiere. Days = 0 => WD / Days = 1 => WE / Days = 2 => SE
        @return void
*/
void control_do_dir_event(int ID_list[32], int mins_since_midnite_list[32], int dim_lvl_list[32], int days);

/**
        @brief Configura el uso de la fotocelula en los modos Reloj Astronomico y Plan Horario.
        @param phc_on indica si debe usarse la fotocelula (1) o no (0).
        @return void
*/
void control_do_use_photocell(int phc_on);

/**
        @brief Escribe en las direcciones de memoria que se le pasan como parametro el momento actual del sistema.
        @param secs_since_midnite_p puntero a la direccion donde se escribira la hora actual expresada en segundos desde la medianoche. Por ejemplo, si la hora del sistema es 00:05:31, *secs_since_midnite = 331.
        @param days_since_ny_p puntero a la direccion donde se escribira la fecha del ano actual expresada en dias desde el ano nuevo. Por ejemplo, si el dia del sistema es 3 de febrero, *days_since_ny = 34.
        @param years_since_2000_p puntero a la direccion donde se escribira el ano actual expresado en anos desde el 2000. Por ejemplo, si el ano del sistema es 2022, *years_since_2000 = 22.
        @return void
*/

void control_do_get_moment(int* secs_since_midnite_p, int* days_since_ny_p, int* years_since_2000_p);

/**
    @brief Escribe en la direccion de memoria que se le pasa como argumento el valor de tension medido en Volts.
    @param volt puntero a la direccion donde se escribira el valor de tension leido.
    @return void
*/

void control_do_get_volt(float* volt);

/**
    @brief Escribe en la direccion de memoria que se le pasa como argumento el valor de corriente medido en Amperes.
    @param curr puntero a la direccion donde se escribira el valor de corriente leido.
    @return void
*/

void control_do_get_curr(float* curr);

/**
    @brief Escribe en la direccion de memoria que se le pasa como argumento el valor de potencia activa medido en Watts.
    @param actpwr puntero a la direccion donde se escribira el valor de potencia leido.
    @return void
*/

void control_do_get_actpwr(float* actpwr);

/**
    @brief Escribe en la direccion de memoria que se le pasa como argumento el valor de potencia activa activa en Watts.
    @param actpwr puntero a la direccion donde se escribira el valor de potencia leido.
    @return void
*/

void control_do_get_apppwr(float* apppwr);

/**
    @brief Escribe en la direccion de memoria que se le pasa como argumento el valor de potencia reactiva medido en Volt*Amperes.
    @param reapwr puntero a la direccion donde se escribira el valor de potencia leido.
    @return void
*/

void control_do_get_reapwr(float* reapwr);

/**
    @brief Escribe en la direccion de memoria que se le pasa como argumento el valor del factor de potencia medido.
    @param pfactor puntero a la direccion donde se escribira el valor de factor de potencia leido.
    @return void
*/

void control_do_get_pfactor(float* pfactor);
//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************

#endif
