/**
 * @file timer.h
 * @brief Este modulo agrupa funciones para el manejo del timer A. Define estructuras referentes al manejo del tiempo.
 * Agrupa funciones para el manejo de estas estructuras.
 *
 * 
 * Modulo timer
 *
 *    
 *	 
 *  
 * 
 *
* timer.c

* @version 2.0
* @author  	Emilio Albarrracin 	<ealbarracin2911@gmail.com>
* @author	Tomas Arrivillaga	<tomas92@ymail.com>
* @author	Felipe Moran		<felipemoran73@gmail.com>
* @date 4 de Agosto 2020
*
* @date 4 de Agosto 2020
**/
//*****************************************************************************
//
//!
//! \addtogroup timer_module
//! @{
//
//*****************************************************************************

#ifndef TIMER
#define TIMER

/**
  @brief estructura basica de tiempo: hora, minutos y segundos. Definicion del tipo type_time.
 */
typedef struct struct_time 
{
	int hours;
	int minutes;
	int seconds;

} type_time ;

/**
 * @brief estructura basica de fecha: ano, meses y dia. Definicion del tipo type_date.
 */
typedef struct struct_date 
{
	int year;
	int month;
	int day;

} type_date ;

/**
 * @brief estructura basica de momento: fecha y hora. Definicion del tipo type_moment.
 */
typedef struct struct_moment
{
	type_date date;
	type_time time;

} type_moment;

/**
 	@brief guarda en una variable privada el modulo el momento que se le pasa.
 	@param min momento que se desea guardar.
 	@return void
*/

void timer_set_moment (type_moment min);

/**
 @brief Busca de una variable privada los dias desde el primero de Enero.
 @param void.
 @return Dias desde el primero de Enero.
*/

int  timer_get_days_since_new_year();

/**
 @brief Calcula si el ano actual es bisiesto.
 @param void.
 @return 1 si es bisiesto, 0 si no lo es.
*/

int  timer_is_leap_year();

/**
 @brief Incrementa en 1s la estructura que se le pasa
 @param type_moment * moment Momento a incrementar.
 @return void
*/

void timer_inc_time_generic(type_moment * moment);

/**
 @brief Llama a timer_inc_time_generic a la variable current_moment.
 @param void
 @return void
*/

void timer_inc_time();


/**
 @brief Aumenta un dia el momento que se le pasa.
 @param moment Momento a aumentar
 @return void
*/
void timer_inc_date_generic(type_moment * moment);

/**
 @brief Aumenta un dia el current_moment.
 @param void
 @return void
*/

void timer_inc_date ();

/**
 @brief Devuelve en el puntero pasado, el momento actual.
 @param m_out Momento donde se quiere el momento actual
 @return void
*/

void timer_get_moment(type_moment* m_out);

/**
 @brief Calcula si el momento actual es fin de semana o no (variacion del metodo de Gauss para calcular dia de la semana)
 @param void
 @return 1 si es fin de semana, 0 si no.
*/

int  timer_is_weekend();

/**
 @brief Revisa la relacion temporal entre el argumento y el momento actual.
 @param mark Momento a revisar
 @return 1 si son iguales, 0 si el momento actual es menor que el argumento y 2 si el momento actual es mayor que el argumento.
*/

int  timer_mark_reached(type_moment mark);

/**
 @brief calcula la cantidad de minutos entre tiempos.
 @param ref_time tiempo base.
 @param other_time tiempo a comparar.
 @return la cantidad de minutos. 
*/

int timer_minutes_between(type_time ref_time , type_time other_time);

/**
	Revisa lo siguiente: 
		ano>2000,
		0<mes<13,
		0<dia< (31,30,29,28)
		dependiendo del mes, 0>=hora>24,
		0>=minutos>60,
		0>=segundos>60,
		0>=milisegundos>1000
 @brief	Revisa que el argumento tiene sentido.
 @param moment_in Momento a revisar.
 @return 1 si es un momento valido, 0 si no. 
 */
int timer_valid_moment(type_moment moment_in);

/**
 @brief incrementa los minutos en time segun la variable minutes.
 @param time puntero al tiempo al que se aumentaran los minutos.
 @param minutes cantidad de minutos a aumentar.
 @return void. 
*/

void timer_time_inc_minutes(type_time* time, int minutes);

/**
 @brief decrementa los minutos en time segun la variable minutes.
 @param time puntero al tiempo al que se decrementaran los minutos.
 @param minutes cantidad de minutos a decrementar.
 @return void. 
*/

void timer_time_dec_minutes(type_time* time, int minutes);
//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************

#endif

