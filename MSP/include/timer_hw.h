/**
 * @file timer_hw.h
 * @brief Modulo que agrupa las funciones de configuracion del funcionamiento del Timer_A y su funci�n de interrupcion.
 *
 * 
 * Modulo timer_hw
 *
 *    
 *	 
 *  
 * 
 *
* timer_hw.c

* @version 2.0
* @author  	Emilio Albarrracin 	<ealbarracin2911@gmail.com>
* @author	Tomas Arrivillaga	<tomas92@ymail.com>
* @author	Felipe Moran		<felipemoran73@gmail.com>
* @date 4 de Agosto 2020
*
* @date 4 de Agosto 2020
**/

//*****************************************************************************
//
//!
//! \addtogroup timer_module
//! @{
//
//*****************************************************************************


#ifndef TIMER_HW
#define TIMER_HW

#include <msp.h>
#include <timer.h>



/**
  @brief configura el timer_A para interrumpir cada 1sec.
  @param void
  @return void
*/
void timer_hw_config_timer_A (void);

//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************
#endif
