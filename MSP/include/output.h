/**
 * @file output.h
 * @brief Este modulo agrupa las funciones que manejan las salidas hacia el circuito de acondicionamiento y el circuito On/Off.
 *
 * 
 * Modulo output
 *
 *    
 *	 
 *  
 * 
 *
* output.c

* @version 2.0
* @author  	Emilio Albarrracin 	<ealbarracin2911@gmail.com>
* @author	Tomas Arrivillaga	<tomas92@ymail.com>
* @author	Felipe Moran		<felipemoran73@gmail.com>
* @date 4 de Agosto 2020
*
* @date 4 de Agosto 2020
**/

//*****************************************************************************
//
//!
//! \addtogroup output_module
//! @{
//
//*****************************************************************************


#ifndef OUTPUT
#define OUTPUT

/**
	@brief luminaria prendida
	Se considera que la luminaria esta "ON" cuando se esta a 100.
 */
#define ON 100
/**
	@brief luminaria apagada
	se considera que la luminaria esta "OFF" cuando se esta a 0.
 */
#define OFF 0

#include <msp.h>
#include <output_hw.h>

/**
 	@brief se llama a la función que configura el tipo de HW que va a ser utilizado.
    @param void
    @return void
 */
void output_config ();

/**
 	@brief se le pasa a la funcion dependiente de HW a cuanto se debe prender la luminaria. 
 		Guarda el nuevo nivel de luz en una variable global.
 	@param duty_cycle nivel de luz deseado.
    @return void
 */
void output_set_light_level (float duty_cycle);

/**
 	@brief se obtiene el nivel de luz actual.
 	@return devuelve el nivel de luz actual.
 */
int output_get_light_level ();
//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************

#endif
