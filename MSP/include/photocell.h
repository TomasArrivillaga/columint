/**
 * @file photocell.h
 * @brief Este modulo agrupa las funciones toman medidas del circuito fotosensible y realizan calculos con ellas.
 *
 * 
 * Modulo photocell
 *
 *    
 *	 
 *  
 * 
 *
 * photocell.c

* @version 2.0
* @author  	Emilio Albarrracin 	<ealbarracin2911@gmail.com>
* @author	Tomas Arrivillaga	<tomas92@ymail.com>
* @author	Felipe Moran		<felipemoran73@gmail.com>
* @date 4 de Agosto 2020
*
* @date 4 de Agosto 2020
**/
//*****************************************************************************
//
//!
//! \addtogroup photocell_module
//! @{
//
//*****************************************************************************
#ifndef INCLUDE_PHOTOCELL_H_
#define INCLUDE_PHOTOCELL_H_

#include <msp.h>
#include <timer.h>
#include <photocell_hw.h>

/** @brief valor de pasos del ADC14 que representa el umbral de oscuridad */
#define LOWERTHRESHOLD 7000
/** @brief valor de pasos del ADC14 que representa el umbral de luz */
#define UPPERTHRESHOLD 9000


#define LOW_AVG 1
#define HIGH_AVG 2
#define MID_AVG 0

/**
  @brief obtiene valores del ADC14 y los acumula en una variable interna del modulo photocell.					
  @param void
  @return void 
*/

void phc_CheckMeasure();

/**
  @brief evalua si el promedio de luz esta por encima del umbral de luz, por debajo del umbral de oscuridad o entre medio de los dos.					
  @param void
  @return 0 si el nivel de luz esta entre medio de los dos umbrales, 1 si esta por debajo del umbral de oscuridad y 2 si esta por encima del umbral de luz.
*/

uint8_t phc_CheckAverage();
//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************
#endif
