/**
 * @file output_hw.h
 * @brief Este modulo agrupa las funciones que manejan las salidas hacia el circuito de acondicionamiento y el circuito On/Off a nivel de hardware.
 *
 * 
 * Modulo output_hw
 *
 *    
 *	 
 *  
 * 
 *
 * output_hw.c

* @version 2.0
* @author  	Emilio Albarrracin 	<ealbarracin2911@gmail.com>
* @author	Tomas Arrivillaga	<tomas92@ymail.com>
* @author	Felipe Moran		<felipemoran73@gmail.com>
* @date 4 de Agosto 2020
*
* @date 4 de Agosto 2020
**/

//*****************************************************************************
//
//!
//! \addtogroup output_module
//! @{
//
//*****************************************************************************

#ifndef OUTPUT_HW
#define OUTPUT_HW

#include <msp.h>
#include <math.h>

/**
 @brief configura los registros del MSP432F401 para tener una salida PWM a 1638Hz en el puerto 2.5.
 @param void
 @return void
 */

void pwm_config ();

/**
 @brief carga en el registro comparador el número necesario para obtener un PWM con Duty Cycle deseado.
 @param duty_cycle numero deseado del Duty Cycle de la salida PWM.
 @return void
 */

void pwm_set_output (float duty_cycle);
//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************

#endif
