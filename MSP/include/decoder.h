 /**
 * @file 	decoder.h														
 * @brief 	funciones que manejan y decodifican los mensajes recibidos por la UART
 *
 * @author  Tomas Arrivillaga 	<tomas92@ymail.com>
 * @author  Emilio Albarracan 	<ealbarracin2911@gmail.com>
 * @author	Felipe Moran		<Felipemoran73@gmail.com>

 * @version 2.0
 *
 * @date 4 Agosto 2020
 *
 */
//*****************************************************************************
//
//!
//! \addtogroup decoder_module
//! @{
//
//*****************************************************************************

#ifndef DECODER
#define DECODER

#include <UART.h>
#include <control.h>

/*Mascaras de decodificacion de bytes*/
#define CMD_MSK 		0xF8
#define DIM_LVL_MSK 	0x1F
#define YRS_MSK 		0x7E

/*Identificadores de plan horario*/
#define WD				0
#define WE  			1
#define SE  			2

/*Para PHC*/
#define PHC_ON 			1
#define PHC_OFF 		0

/*Identificadores de comandos*/
#define SET_ON 			0x00
#define SET_OFF 		0x01
#define SET_DIM 		0x02
#define ADD_EVENT_WE 	0x03
#define ADD_EVENT_WD 	0x04
#define RES_EVENT_WE 	0x05
#define RES_EVENT_WD 	0x06
#define DIR_EVENT_WE 	0x07
#define DIR_EVENT_WD 	0x08
#define DIR_EVENT_SE 	0x09
#define DEL_EVENT_WE 	0x0A
#define DEL_EVENT_WD 	0x0B
#define SET_MODE 		0x0C
#define SET_MOMENT 		0x0D
#define USE_PHC_ON 		0x0E
#define USE_PHC_OFF 	0x0F
#define GET_MOMENT 		0x10
#define GET_MODE 		0x11
#define GET_VOLT		0x12
#define GET_CURR 		0x13
#define GET_ACTPWR 		0x14
#define GET_REAPWR 		0x15
#define GET_APPPWR 		0x16
#define GET_PFACTOR 	0x17

/**
 @brief bandera usada para señalar que el mensaje se terminó de enviar.
 */
extern int TxMessageComplete;
/**
 @brief bandera usada para signalizar que se quiere usar la fotocelula.
 */
extern int flag_use_photocell;
/**
 @brief modo en el que se encuentra el controlador.
 */
extern type_mode current_mode;
/**
 @brief bandera usada para enviar un mensaje a traves del Main.
 */
extern uint8_t lora_begin_tx;
/**
 @brief bandera usada para señalar que hay un nuevo día.
 */
extern int new_day;

/**
 @brief toma el mensaje crudo de la UART para decodificar y tomar una acción. Tambien codifica y envia por UART las respuestas.
 @param char* buffer: puntero donde se encuentra el mensaje recibido por la UART.
 @return void
 */
uint8_t decoder_decode_RX(char* buffer);
//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************

#endif
