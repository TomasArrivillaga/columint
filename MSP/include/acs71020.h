/**
 * @file acs71020.h
 * @brief Este modulo agrupa las funciones que obtienen las medidas realizadas por el ACS71020.
 *
 * 
 * Modulo acs71020
 *
 *    
 *	 
 *  
 * 
 *
* acs71020.c

* @version 2.0
* @author  	Emilio Albarrracin 	<ealbarracion2911@gmail.com>
* @author	Tomas Arrivillaga	<tomas92@ymail.com>
* @author	Felipe Moran		<felipemoran73@gmail.com>
* @date 4 de Agosto 2020
*
* @date 4 de Agosto 2020
**/

//*****************************************************************************
//
//!
//! \addtogroup acs71020_module
//! @{
//
//*****************************************************************************

#ifndef INCLUDE_ACS71020_H_
#define INCLUDE_ACS71020_H_

/**
  @brief obtiene la tension medida por el ACS71020.					
  @param void
  @return valor de tension obtenido. 
*/

float acs71020_get_voltage();

/**
  @brief obtiene la corriente medida por el ACS71020.					
  @param void
  @return valor de corriente obtenido. 
*/

float acs71020_get_current();

/**
  @brief obtiene la potencia activa medida por el ACS71020.					
  @param void
  @return valor de potencia obtenido. 
*/

float acs71020_get_actpwr();

/**
  @brief obtiene la potencia aparente medida por el ACS71020.					
  @param void
  @return valor de potencia obtenido. 
*/

float acs71020_get_apppwr();

/**
  @brief obtiene la potencia reactiva medida por el ACS71020.					
  @param void
  @return valor de potencia obtenido. 
*/

float acs71020_get_reapwr();

/**
  @brief obtiene el factor de potencia medido por el ACS71020.					
  @param void
  @return valor de factor de potencia obtenido. 
*/

float acs71020_get_pfactor();
//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************

#endif /* INCLUDE_ACS71020_H_ */
