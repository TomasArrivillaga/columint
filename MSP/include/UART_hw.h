/**
 * @file UART_hw.h														
 * @brief funciones que controlan el HW para la comunicacion UART.
 *
 * @author  Tomas Arrivillaga 	<tomas92@ymail.com>
 * @author  Emilio Albarracin 	<ealbarracin2911@gmail.com>
 * @author	Felipe Moran		<Felipemoran73@gmail.com>

 * @version 2.0
 *
 * @date 4 de Agosto de 2020
 *
 */

#ifndef UART_HW
#define UART_HW

#include <msp.h>
#include <uart.h>
#include <gpio.h>
#include <rom_map.h>

//*****************************************************************************
//
//!
//! \addtogroup uart_module
//! @{
//
//*****************************************************************************

/**
 @brief define el largo posible de caracteres en un mensaje.
 */
#define MAX_SIZE_MSG 50


void handle_Rx(char *extbuff);
void beginTx(char *extBuff);
/**
 @brief configura los registros de la placa MSP432P401R para tener una comunicacion serie a 9600 baudios utilizando el periferico EUSCIA0.
 @param void
 @return void
 */
void UART_hw_config(void);

/**
 @brief carga en el buffer de transmision el primer caracter a cargar y habilita las interrupciones.
 @param char a primer caracter a cargar en el buffer de transmision.
 @return void
 */
void UART_hw_first_bit_TX(char a);
//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************

#endif
