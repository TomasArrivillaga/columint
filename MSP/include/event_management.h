/**
 * @file event_management.h
 * @brief Este modulo agrupa las funciones que configuran y manejan los eventos.
 *
 * 
 * Modulo event_management
 *
 *    
 *	 
 *  
 * 
 *
* event_management.c

* @version 2.0
* @author  	Emilio Albarrracin 	<ealbarracion2911@gmail.com>
* @author	Tomas Arrivillaga	<tomas92@ymail.com>
* @author	Felipe Moran		<felipemoran73@gmail.com>
* @date 4 de Agosto 2020
*
* @date 4 de Agosto 2020
**/
//*****************************************************************************
//
//!
//! \addtogroup event_management_module
//! @{
//
//*****************************************************************************

#ifndef INCLUDE_EVENT_MANAGEMENT_H

#define INCLUDE_EVENT_MANAGEMENT_H

#include <timer.h>
#include <sun_equation.h>
#include <control.h>

/** @brief numero maximo de eventos */
#define MAX_SP_EVENTS 32


/** @brief definicion de tipo de los eventos*/
typedef struct struct_event
{
 type_time event_time;
 int dim_lvl;
} type_event;


/** @brief indice del proximo evento: astronomical clk*/
int next_event_index_AC; 

/** @brief indice del proximo evento: schedule plan*/
int next_event_index_SP; 


/**
  @brief obtiene la hora del amanecer y la puesta del sol para almacenarlos en el arreglo
  de eventos solares.					
  @param void
  @return void 
*/

void event_new_sun_events();

/**
  @brief retorna el nivel de dimerizacion del evento indicado por "index" en el arreglo correspondiente
  al modo "mode"					
  @param mode modo correspondiente al nivel de dimerizacion que se desea obtener
  @param index indice en el arreglo
  @return nivel de dimerizacion 
*/

int event_get_dim_level(type_mode mode, int index);

/**
  @brief retorna el indice en el arreglo de eventos del proximo evento en el dia para un modo dado.
  @param mode_in modo del cual se quiere saber el proximo evento.
  @return indice en el arreglo 
*/

int event_calculate_next_index (type_mode mode_in);

/**
  @brief retorna el indice en el arreglo de eventos del ultimo evento occurido en lo que va del dia para un modo dado.
  @param mode_in modo del cual se quiere saber el ultimo evento ocurrido.
  @return nivel de dimerizacion 
*/

int event_calculate_current_index (type_mode mode_in);

/**
  @brief indica si hay un evento en el minuto actual para un modo dado.
  @param mode modo para el cual se desea saber si ha llegado un evento.
  @return 1 si se llego un evento, 0 si no. 
*/

int event_reached(type_mode mode);

/**
  @brief obtiene el indice en el arreglo de eventos del proximo evento en el dia para un modo dado.
   y lo almacena en la variable "next_event_index_SP" o "next_event_index_AC" segun sea "mode".
  @param mode modo para el cual se desea obtener el indice.
  @return void. 
*/

void event_save_index(type_mode mode);

/**
  @brief inicializa los arreglos de eventos y obtiene y almacena en "next_event_index_SP" o "next_event_index_AC"
   los indices correspondientes a los distintos modos.  
  @param void.
  @return void. 
*/

void event_init();

/**
  @brief elimina el evento indicado por su indice, en el arreglo indicado por day_is_weekend.
  @param day_is_weekend vale 1 si se desea eliminar un evento del arreglo de fin de semana,
   0 si se desea eliminar un evento del arreglo de entre semana.
  @param index indice del evento que se desea eliminar. 
  @return void 
*/

void event_remove (int day_is_weekend, int index);

/**
  @brief agrega el evento indicado por "new_event", en el arreglo indicado por day_is_weekend. 
  @param day_is_weekend vale 1 si se desea agregar un evento del arreglo de fin de semana,
   0 si se desea agregar un evento del arreglo de entre semana.
  @param new_event evento que se desea agregar. 
  @return void 
*/

void event_add (int day_timer_is_weekend, type_event new_event);

/**
  @brief elimina todos los eventos de uo de los arreglos de eventos del plan horario. Se resetea el 
   arreglo indicado por "day_is_weekend".
  @param day_is_weekend vale 1 si se desea resetear el arreglo de fin de semana,
   0 si se desea resetear el arreglo de entre semana.
  @return void. 
*/

void event_reset (int day_is_weekend);

/**
  @brief Crea un tipo evento (type_event) a partir de la hora, minutos, segundos y nivel de dimerizacion. 
  @param h horas.
  @param m minutos.
  @param d nivel de dimerizacion.
  @return evento generado. 
*/

type_event event_create(int h, int m, int d);

/**
  @brief Guarda en el tipo evento "event" los campos del evento del arreglo
  "week_events" o "weekend_events" (dependiendo de si es fin de semana o no) del lugar indicado por
  el indice "index" .
  @param event puntero al evento donde se guardar� la informaci�n.
  @param timer_is_weekend indica si se refiere al arreglo de fin de semana o de entre semana.
  @param index indice que indica qu� evento del arreglo se copiar�.
  @return void 
*/

void event_get(type_event* event,int timer_is_weekend, int index);

/**
  @brief Indica si actualmente es de dia o de noche. 
  @param void
  @return 1 si es de dia, 0 si es de noche 
*/

int event_is_day();

/**
  @brief retorna el nivel de dimerizacion del proximo evento en el dia para el modo "mode". 
  @param mode modo del cual se quiere obtener la dimerizacion del siguiente evento.
  @return nivel de dimerizacion 
*/
int event_get_next_dim_level(type_mode mode);
//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************


#endif /** INCLUDE_EVENT_MANAGEMENT_H_ */
