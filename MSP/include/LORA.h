/**
 * @file LORA.h
 * @brief Este modulo agrupa las funciones que configuran y manejan la comunicacion LoRa.
 *
 * 
 * Modulo LoRa
 *
 *   
 *   
 *  
 * 
 *
* lora.c

* @version 2.0
* @author Emilio Albarrracin  <ealbarracion2911@gmail.com>
* @author Tomas Arrivillaga <tomas92@ymail.com>
* @author Felipe Moran <felipemoran73@gmail.com>
* @date 4 de Agosto 2020
*
* @date 4 de Agosto 2020
**/

//*****************************************************************************
//
//!
//! \addtogroup LoRa_module
//! @{
//
//*****************************************************************************

#ifndef INCLUDE_LORA_H_
#define INCLUDE_LORA_H_

#include <lora_hw.h>
#include <string.h>
#include <utils.h>

/**
  @brief Resetea el RN2903 y establece los parametros APPEUI, DEVEUI y APPKEY. Luego se activa en la red con un Join OTAA.
  @param void.
  @return void. 
*/

void loraInit(void);

/**
  @brief Activa los canales 0 al 7 y desactiva del 8 al 63 de la comunicacion Lora
  @param void.
  @return void. 
*/

void loraChannelSelect(void);

/**
  @brief identifica si el mensaje en extBuff corresponde a un downlink reportado por el RN2903. En caso de serlo, obtiene el payload a partir del mensaje recibido desde el RN2903.
  @param extBuff buffer que contiene el mensaje recibido del RN2903. Si el mensaje es efectivamente un downlink, la funcion escribe el payload del mismo en este buffer. 
  @return 1 si el mensaje en extBuff era efectivamente un downlink, 0 en otro caso. 
*/

uint8_t loraDownlink(char* extBuff);

/**
  @brief  Envia un comando via UART al RN2903. Es usada para todos los mensajes cuyo string es estatico y la respuesta debe ser ok para continuar.
  @param extBuff Buffer con el comando que se desea enviar al RN2903.
  @return void 
*/

void sendCommandAndWaitForOk (char* stringToSend);

/**
  @brief Realiza un uplink 
  @param bytes buffer con el mensaje a enviar por LoRa.
  @param byteqty cantidad de bytes del mensaje a enviar por LoRa.
  @return void. 
*/

void loraSendBytes (char* bytes, uint8_t byteqty);
//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************

#endif /* INCLUDE_LORA_H_ */
