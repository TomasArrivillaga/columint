/**
 * @file lora_hw.h
 * @brief Este modulo agrupa las funciones que configuran y manejan el hardware asociado a la comunicacion LoRa.
 *
 * 
 * Modulo lora_hw
 *
 *    
 *	 
 *  
 * 
 *
* lora_hw.c

* @version 2.0
* @author  	Emilio Albarrracin 	<ealbarracion2911@gmail.com>
* @author	Tomas Arrivillaga	<tomas92@ymail.com>
* @author	Felipe Moran		<felipemoran73@gmail.com>
* @date 4 de Agosto 2020
*
* @date 4 de Agosto 2020
**/

//*****************************************************************************
//
//!
//! \addtogroup LoRa_module
//! @{
//
//*****************************************************************************

#ifndef INCLUDE_LORA_HW_H_
#define INCLUDE_LORA_HW_H_

#include <driverlib.h>

/** @brief numero maximo de bytes en un mensaje LoRa */
#define MAX_SIZE_loraHW_MSG 50



/**
  @brief Configura los puertos a ser usados como UART para comunicarse con el RN2903.					
  @param void
  @return void 
*/

void loraHW_init(void);

/**
  @brief realiza una transmision via UART hacia el RN2903.					
  @param extBuff mensaje a enviar.
  @return void 
*/

void loraHWSendCommand(char* extBuff);

/**
  @brief recibe las respuestas a los comandos enviados por UART					
  @param extbuff buffer en el que se carga la respuesta al comando.
  @return void 
*/

void loraHWCommandResponse(char* extbuff);

/**
  @brief indica si se recibio algo por la UART					
  @param void.
  @return 1 si se recibio algo, 0 en otro caso.
*/

uint8_t loraHWRxAvailable();

/**
  @brief indica si se termino la ultima transmision via UART. 					
  @param void.
  @return 1 si la utima transmision se completo, 0 en otro caso.
*/

uint8_t loraHWTxAvailable();
//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************
#endif /* INCLUDE_loraHW_H_ */
