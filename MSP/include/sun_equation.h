/**
 * @file sun_equation.h
 * @brief Este modulo agrupa las funciones que calculan la hora de la salida y puesta del sol.
 *
 * 
 * Modulo sun_equation
 *
 *    
 *	 
 *  
 * 
 *
* sun_equation.c

* @version 2.0
* @author  	Emilio Albarrracin 	<ealbarracin2911@gmail.com>
* @author	Tomas Arrivillaga	<tomas92@ymail.com>
* @author	Felipe Moran		<felipemoran73@gmail.com>
* @date 4 de Agosto 2020
*
* @date 4 de Agosto 2020
**/

//*****************************************************************************
//
//!
//! \addtogroup sun_equation_module
//! @{
//
//*****************************************************************************

#ifndef SUN_EQUATION
#define SUN_EQUATION

#include <timer.h>
#include <math.h>


/**
 @brief Copia el valor del atardecer al puntero que se le pasa.
 @param sunset_aux puntero donde se guarda la hora del amanecer
 @return void
*/

void sun_equation_get_sunset(type_time* sunset_aux);

/**
 @brief Copia el valor del amanecer al puntero que se le pasa.
 @param sunrise_aux puntero donde se guarda la hora del atardecer.
 @return void
*/

void sun_equation_get_sunrise(type_time* sunrise_aux);

/**
 @brief Obtiene el tiempo en que sale y se pone el sol y las guarda en variables privadas al modulo.
 @param lat valor de la latitud donde se quiere calcular los tiempo. Positivo para Norte, negativo para Sur.
 @param lon valor de la longitud donde se quiere calcular los tiempo. Positivo para Este, negativo para Oeste.
 @param time_zone diferencia horaria con el GMT para entregar los eventos en la hora local.
 @return void
*/
void sun_equation_calc_sun_events(float lat,float lon, int time_zone);
//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************

#endif
