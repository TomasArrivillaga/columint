/**
* @file main.c
*/

#include <msp.h>
#include <decoder.h>
#include <timer.h>
#include <timer_hw.h>
#include <output.h>
#include <photocell.h>
#include <event_management.h>
#include <sun_equation.h>
#include <control.h>
#include <UART_hw.h>
#include <driverlib.h>
#include <acs71020_hw.h>
#include <acs71020.h>
#include <lora_HW.h>
#include <LORA.h>
#include <utils.h>

extern int new_minute, new_day, new_second;
uint8_t lora_begin_tx=0;

uint8_t response_size;

char extBuff[MAX_SIZE_MSG];

type_moment zero ={{2020,8,7},{16,04,20}};       //YYYY/MM/DD


int main(void)

{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer
    RTCPS1CTL |=RT1PSIE;


    UART_hw_config();
    timer_hw_config_timer_A();
    phc_init();
    output_config();
    timer_set_moment(zero);
    sun_equation_calc_sun_events(-32.5228,-55.7658,-3); //   lat: S negativo, N positivo ; W negativo, E positivo
    event_init();
    acs71020_config();
    loraHW_init();




    __NVIC_EnableIRQ(TA0_0_IRQn );  //habilita interrupciones del timerA0
	__NVIC_EnableIRQ(TA1_0_IRQn );  //habilita interrupciones del timerA1
	__NVIC_EnableIRQ(PORT5_IRQn);   //habilita interrupciones del port5
	__NVIC_EnableIRQ(ADC14_IRQn);
    MAP_Interrupt_enableInterrupt(INT_EUSCIA0); //habilita interrupciones uart
    MAP_Interrupt_enableInterrupt(INT_EUSCIA2); //habilita interrupciones uart
    __enable_interrupt();           //habilita interrupciones globales

    loraInit();

	while (1)
	{

	    if (loraHWTxAvailable() && lora_begin_tx){
	        lora_begin_tx=0;
            loraChannelSelect();
            if(response_size != 0){
                loraSendBytes(extBuff,response_size);
            }
	    }

        if (loraHWRxAvailable())
        {
            __disable_interrupt();
            loraHWCommandResponse(extBuff);
            __enable_interrupt();

            if(loraDownlink(extBuff))
            {
                response_size=decoder_decode_RX(extBuff);
            }
        }

        if(new_day)
        {
            new_day=0;
            sun_equation_calc_sun_events(-32.5228,-55.7658,-3);
            event_new_sun_events();
            event_save_index(SCHEDULE_PLAN);
        }

        if(new_minute)
        {

            new_minute=0;
            control_take_action();
            loraChannelSelect();
            sendCommandAndWaitForOk("mac tx uncnf 50 FF\r\n");
        }
        if(new_second){
            new_second=0;
            phc_CheckMeasure();
        }

	}
	return 0;
}


