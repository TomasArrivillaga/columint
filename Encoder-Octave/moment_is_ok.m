## Copyright (C) 2020 felip
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} moment_is_ok (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: felip <felip@LAPTOP-3L79L74G>
## Created: 2020-06-01

function is_ok = moment_is_ok(y,mo,d,h,mi,s)
  is_ok =1;
  if ((s<0) || s>59)
    is_ok =0;
  endif
  if ((mi<0) || mi>59)
    is_ok =0;
  endif
  if ((h<0) || h>24)
    is_ok =0;
  endif
  if ((y<2000) || y>2063)
    is_ok =0;
  endif
  if ((mo<1) || mo>12)
    is_ok =0;
  endif
  
  if (is_leap_year(y))
    month_days = [31,29,31,30,31,30,31,31,30,31,30,31];
  else
    month_days = [31,29,31,30,31,30,31,31,30,31,30,31];
  endif
  if (mo>0 && mo<13)
    if ((d<1) || d> month_days(1,mo))
      is_ok =0;
    endif
  endif
endfunction
