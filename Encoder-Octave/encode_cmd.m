## Copyright (C) 2020 felip
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} encode_cmd (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: felip <felip@LAPTOP-3L79L74G>
## Created: 2020-06-01

function code = encode_cmd (cmd)
  knowncommand = 0;
  % -------------------- SET ON -------------------------------------
  if ((length(cmd) == length("SET ON")) && strcmp(cmd,"SET ON"))
    knowncommand = 1;
    code = "00";
  endif
  % ------------------------- SET OFF ---------------------------------
  if ((length(cmd) == length("SET OFF")) && strcmp(cmd,"SET OFF"))
    knowncommand = 1;
    code = "08";
  endif
  % ----------------------- SET DIM XX ---------------------------------
  if ((length(cmd) == length("SET DIM XXX")) && strcmp(cmd(1:7),"SET DIM"))
    knowncommand = 1;
    dim = str2num(cmd(1,9:11));
    if dim < 15
      dimhex = cstrcat("0", dec2hex(dim));
    else  
      dimhex = dec2hex(dim);
    endif
    if (dim_is_ok(dim))  
      code = cstrcat("10"," ", dimhex);
    else
      printf("INVALID LIGHT LEVEL");
      code = -1;
    endif
  endif 
  %----------------------- ADD EVENT DT HH:MM XXX ----------------------------
  if ((length(cmd) == length("ADD EVENT DT HH:MM XXX")) && strcmp(cmd(1,1:9),"ADD EVENT"))
    knowncommand = 1;
    h = str2num(cmd(1,14:15));
    m = str2num(cmd(1,17:18));
    dim = str2num(cmd(1,20:22));
    henmins = 60*h + m;
    datadec = round(dim/5)+32*henmins;
    datahex = dec2hex(datadec);
    while (length(datahex)<4)
      datahex = cstrcat("0",datahex);
    endwhile
    if (cmd(1,11:12) == "WE")
      code = cstrcat("18"," ",datahex(1:2)," ",datahex(3:4));
    elseif  (cmd(1,11:12) == "WD")
      code = cstrcat("20"," ",datahex(1:2)," ",datahex(3:4));
    else
      knowncommand = 0;
    endif
  endif
  %----------------------- RES EVENT WE ----------------------------
  if ((length(cmd) == length("RES EVENT WE")) && strcmp(cmd(1,1:12),"RES EVENT WE"))
    knowncommand = 1;
    code = "28";
  endif
  %----------------------- RES EVENT WD ----------------------------  
  if ((length(cmd) == length("RES EVENT WD")) && strcmp(cmd(1,1:12),"RES EVENT WD"))
    knowncommand = 1;
    code = "30";
  endif
  %----------------------- DIR EVENT WE -------------------------------
  if ((length(cmd) == length("DIR EVENT WE")) && strcmp(cmd(1,1:12),"DIR EVENT WE"))
    knowncommand = 1;
    code = "38";
  endif
  %----------------------- DIR EVENT WD ---------------------------------
  if ((length(cmd) == length("DIR EVENT WD")) && strcmp(cmd(1,1:12),"DIR EVENT WD"))
    knowncommand = 1;
    code = "40";
  endif
  %----------------------- DIR EVENT SE ------------------------------------
  if ((length(cmd) == length("DIR EVENT SE")) && strcmp(cmd(1,1:12),"DIR EVENT SE"))
    knowncommand = 1;
    code = "48";
  endif
  %------------------------ DEL EVENT WE ID ---------------------------------
  if ((length(cmd) == length("DEL EVENT WE ID")) && strcmp(cmd(1,1:12),"DEL EVENT WE"))
    knowncommand = 1;
    id = str2num(cmd(1,14:15));
    if id> 0xF
      code = cstrcat("50"," ",dec2hex(id));
    else 
      code = cstrcat("50"," ","0",dec2hex(id));
    endif
  endif
  %------------------------ DEL EVENT WD ID ---------------------------------
  if ((length(cmd) == length("DEL EVENT WD ID")) && strcmp(cmd(1,1:12),"DEL EVENT WD"))
    knowncommand = 1;
    id = cmd(1,14:15);
    if id > 0xF
      code = cstrcat("58"," ",dec2hex(cmd(1,14:15)));
    else 
      code = cstrcat("58"," ","0",dec2hex(cmd(1,14:15)));
    endif
  endif
  %------------------------ SET MODE X ---------------------------------
  if ((length(cmd) == length("SET MODE X")) && strcmp(cmd(1,1:8),"SET MODE"))
    knowncommand = 1;
    mode = str2num(cmd(1,10));
    if (mode_is_ok(mode))
      code = dec2hex(96 + mode);
    else
      code = -1;
      printf("INVALID MODE");
    endif
  endif
  %------------------------ SET MOMENT DD/MM/YYYY HH:MM:SS ---------------------------------
  if ((length(cmd) == length("SET MOMENT DD/MM/YYYY HH:MM:SS")) && strcmp(cmd(1,1:10),"SET MOMENT"))
    knowncommand = 1;
    d = str2num(cmd(1,12:13));
    mo = str2num(cmd(1,15:16));
    y = str2num(cmd(1,18:21));
    h = str2num(cmd(1,23:24));
    mi = str2num(cmd(1,26:27));
    s = str2num(cmd(1,29:30));
    if (moment_is_ok(y,mo,d,h,mi,s))
        time_in_secs = s+60*mi+(60^2)*h;
        years_since_2000 = y-2000;
        if (is_leap_year(y))
          month_days = [31,29,31,30,31,30,31,31,30,31,30,31];
        else
          month_days = [31,29,31,30,31,30,31,31,30,31,30,31];
        endif
        if (mo == 1)
          days_since_ny = d;  
        else
          days_since_ny = sum(month_days(1:mo-1)) + d;
        endif
        databin = cstrcat(dec2bin(time_in_secs,17),dec2bin(years_since_2000,6),dec2bin(days_since_ny,9));
        byte1 = dec2hex(bin2dec(databin(1:8)),2);
        byte2 = dec2hex(bin2dec(databin(9:16)),2);
        byte3 = dec2hex(bin2dec(databin(17:24)),2);
        byte4 = dec2hex(bin2dec(databin(25:32)),2);
        code = cstrcat("68"," ",byte1," ",byte2," ",byte3," ",byte4);      
    else
      code = -1;
      printf("INVALID MOMENT");
    endif
  endif
  %------------------------ USE PHC ON ---------------------------------
  if ((length(cmd) == length("USE PHC ON")) && strcmp(cmd(1,1:10),"USE PHC ON"))
    knowncommand = 1;
    code = "70";
  endif
  %------------------------ USE PHC OFF ---------------------------------
  if ((length(cmd) == length("USE PHC OFF")) && strcmp(cmd(1,1:11),"USE PHC OFF"))
    knowncommand = 1;
    code = "78";
  endif
   %------------------------ GET MOMENT ---------------------------------
  if ((length(cmd) == length("GET MOMENT")) && strcmp(cmd(1,1:10),"GET MOMENT"))
    knowncommand = 1;
    code = "80";
  endif
  %------------------------ GET MODE ---------------------------------
  if ((length(cmd) == length("GET MODE")) && strcmp(cmd(1,1:8),"GET MODE"))
    knowncommand = 1;
    code = "88";
  endif
  %------------------------ GET VOLT ---------------------------------
  if ((length(cmd) == length("GET VOLT")) && strcmp(cmd(1,1:8),"GET VOLT"))
    knowncommand = 1;
    code = "90";  
  endif
  %------------------------ GET CURR  ---------------------------------
  if ((length(cmd) == length("GET CURR")) && strcmp(cmd(1,1:8),"GET CURR"))
    knowncommand = 1;
    code = "98";  
  endif
  %------------------------ GET ACTPWR  ---------------------------------
  if ((length(cmd) == length("GET ACTPWR")) && strcmp(cmd(1,1:10),"GET ACTPWR"))
    knowncommand = 1;
    code = "A0";  
  endif
  %------------------------ GET REAPWR  ---------------------------------
  if ((length(cmd) == length("GET REAPWR")) && strcmp(cmd(1,1:10),"GET REAPWR"))
    knowncommand = 1;
    code = "A8";  
  endif
  if ~knowncommand
    code =-1;
    printf("UNKNOWN COMMAND")
  endif

endfunction
